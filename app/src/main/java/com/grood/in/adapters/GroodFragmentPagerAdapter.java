package com.grood.in.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.grood.in.GroodApp;
import com.grood.in.R;
import com.grood.in.fragments.CategoryFragment_;
import com.grood.in.fragments.ConfirmOderFragment_;
import com.grood.in.fragments.OffersFragment;
import com.grood.in.util.constant.Global;

/**
 * Created by USER on 09-08-2015.
 */
public class GroodFragmentPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 3;
    private String tabTitles[] = new String[] {
            GroodApp.getAppContext().getString(R.string.categories),
            GroodApp.getAppContext().getString(R.string.offers),
            GroodApp.getAppContext().getString(R.string.cart)};
    private Context context;

    public GroodFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 :
                CategoryFragment_ fragment = CategoryFragment_.newInstance();
                return fragment;

            case 1:
                return OffersFragment.newInstance();

            case 2:
                return ConfirmOderFragment_.newInstance(Global.INTENT_FROM_TABS);

        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
