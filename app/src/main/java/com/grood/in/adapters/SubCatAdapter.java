package com.grood.in.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.grood.in.R;
import com.grood.in.customviews.CustomTextView;
import com.grood.in.models.Category;

import java.util.List;

/**
 * Created by USER on 10-08-2015.
 */
public class SubCatAdapter extends ArrayAdapter<Category>{
    private Context mContext;
    private int RESOURCE;
    private  List<Category>  mCategoryList;

    public SubCatAdapter(Context context, int resource, List<Category> objects) {
        super(context, resource, objects);
        mCategoryList  = objects;
        mContext = context;
        RESOURCE = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder vh = null;
        if(view == null) {
            LayoutInflater inflater =
                    (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(RESOURCE, parent,false);
            vh  = new ViewHolder();
            vh.txtSubCatName = (CustomTextView)view.findViewById(R.id.txtSubcategoryName);
            view.setTag(vh);
        }else {
            vh = (ViewHolder)view.getTag();
        }
        vh.txtSubCatName.setText(mCategoryList.get(position).getName());
        return view;
    }

    static class ViewHolder {
        CustomTextView txtSubCatName;
    }
}
