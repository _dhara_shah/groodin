package com.grood.in.adapters;

import android.content.Context;
import android.graphics.drawable.shapes.RoundRectShape;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.grood.in.R;
import com.grood.in.customviews.CustomTextView;
import com.grood.in.models.OrderHistory;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.List;

/**
 * Created by USER on 23-08-2015.
 */
public class OrderHistoryAdapter extends ArrayAdapter<OrderHistory> {
    private Context mContext;
    private int RESOURCE;
    private List<OrderHistory> orderHistoryList;
    private DecimalFormat decimalFormat;

    public OrderHistoryAdapter(Context context, int resource, List<OrderHistory> objects) {
        super(context, resource, objects);
        mContext = context;
        RESOURCE = resource;
        orderHistoryList = objects;
        decimalFormat = new DecimalFormat("#.##");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder vh =null;
        if(view == null) {
            LayoutInflater inflater =
                    (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(RESOURCE, parent, false);
            vh = new ViewHolder();
            vh.txtOrderDate = (CustomTextView)view.findViewById(R.id.txtOrderDate);
            vh.txtOrderRef = (CustomTextView)view.findViewById(R.id.txtOrderRef);
            vh.txtModeOfPayment = (CustomTextView)view.findViewById(R.id.txtPaymentMode);
            vh.txtOrderPrice = (CustomTextView)view.findViewById(R.id.txtTotalPrice);
            vh.txtOrderStatus =(CustomTextView) view.findViewById(R.id.txtOrderStatus);
            view.setTag(vh);
        }else {
            vh = (ViewHolder)view.getTag();
        }

        OrderHistory order = orderHistoryList.get(position);
        vh.txtOrderPrice.setText(decimalFormat.format(Double.parseDouble(order.getTotalPaid())));
        vh.txtOrderStatus.setText(order.getOrderState());
        vh.txtOrderPrice.setText(order.getTotalPaid());
        vh.txtOrderRef.setText(order.getReference());
        vh.txtModeOfPayment.setText(order.getPayment());

        return view;
    }

    static class ViewHolder {
        CustomTextView txtOrderRef;
        CustomTextView txtModeOfPayment;
        CustomTextView txtOrderPrice;
        CustomTextView txtOrderStatus;
        CustomTextView txtOrderDate;
    }
}
