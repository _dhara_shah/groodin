package com.grood.in.adapters;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.grood.in.R;
import com.grood.in.customviews.CustomTextView;
import com.grood.in.models.Address;

import java.util.List;

/**
 * Created by USER on 23-08-2015.
 */
public class AddressAdapter extends ArrayAdapter<Address>{
    private Context mContext;
    private int RESOURCE;
    private List<Address> addressList;

    public AddressAdapter(Context context, int resource, List<Address> objects) {
        super(context, resource, objects);
        mContext = context;
        RESOURCE = resource;
        addressList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder vh = null;

        if(view== null){
            LayoutInflater inflater =
                    (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(RESOURCE, parent,false);
            vh = new ViewHolder();
            vh.txtAddress1 = (CustomTextView)view.findViewById(R.id.txtAddressOne);
            vh.txtAddress2 = (CustomTextView)view.findViewById(R.id.txtAddressTwo);
            vh.txtCity = (CustomTextView)view.findViewById(R.id.txtCity);
            vh.txtCountry  = (CustomTextView)view.findViewById(R.id.txtCountry);
            vh.txtName = (CustomTextView)view.findViewById(R.id.txtName);
            vh.txtNumber = (CustomTextView)view.findViewById(R.id.txtNumber);
            vh.txtZipcode = (CustomTextView)view.findViewById(R.id.txtZipcode);
            view.setTag(view);
        }else {
            vh = (ViewHolder)view.getTag();
        }

        Address address = addressList.get(position);
        vh.txtName.setText(address.getFirstName() +  " " + address.getLastName());
        vh.txtAddress1.setText(address.getAddress1());
        vh.txtAddress2.setText(address.getAddress2());
        vh.txtNumber.setText(address.getMobileNumber());
        vh.txtCity.setText(address.getCity());
        vh.txtZipcode.setText(address.getPostcode());
        vh.txtCountry.setVisibility(View.GONE);

        return view;
    }

    static class ViewHolder {
        CustomTextView txtName;
        CustomTextView txtAddress1;
        CustomTextView txtAddress2;
        CustomTextView txtCity;
        CustomTextView txtZipcode;
        CustomTextView txtNumber;
        CustomTextView txtCountry;
    }
}
