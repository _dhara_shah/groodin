package com.grood.in.adapters;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.grood.in.R;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.customviews.CustomTextView;
import com.grood.in.models.Product;
import com.grood.in.util.CommonUtilities;

import java.util.List;

/**
 * Created by user on 10/08/2015.
 */
public class ProductAdapter extends ArrayAdapter<Product>{
    private int RESOURCE;
    private Context mContext;
    private List<Product> mProductList;
    private CustomDialog.PopupMenuListener mListener;

    public ProductAdapter(Context context, int resource, List<Product> objects,
                          CustomDialog.PopupMenuListener listener) {
        super(context, resource, objects);
        mContext = context;
        RESOURCE = resource;
        mProductList = objects;
        mListener = listener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder vh = null;
        if(view == null) {
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(RESOURCE, parent ,false);
            vh = new ViewHolder();
            vh.txtProductName = (CustomTextView)view.findViewById(R.id.txtProductName);
            vh.txtProductPrice = (CustomTextView)view.findViewById(R.id.txtPrice);
            vh.imgOverflow = (ImageView)view.findViewById(R.id.imgOverflow);
            vh.imgProduct = (ImageView)view.findViewById(R.id.imgProduct);
            view.setTag(vh);
        }else {
            vh = (ViewHolder)view.getTag();
        }

        vh.imgOverflow.setTag(String.valueOf(position));
        Product product = mProductList.get(position);
        vh.txtProductName.setText(product.getProductName());
        vh.txtProductPrice.setText(" र "  + product.getProductPrice());

        vh.imgOverflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Product prodObj = mProductList.get(Integer.parseInt((String) v.getTag()));
                CustomDialog.showPopupMenu(mContext, v, prodObj, mListener);
            }
        });

        CommonUtilities.getAQInstance()
                .id(vh.imgProduct)
                .image("https://"+product.getProductImage(), true, true, 200, 0);

        return view;
    }

    static class ViewHolder {
        CustomTextView txtProductName;
        CustomTextView txtProductPrice;
        ImageView imgOverflow;
        ImageView imgProduct;
    }
}
