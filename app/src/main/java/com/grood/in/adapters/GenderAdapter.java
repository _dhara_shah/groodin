package com.grood.in.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

import com.grood.in.R;
import com.grood.in.customviews.CustomTextView;

import java.util.List;

/**
 * Created by USER on 09-08-2015.
 */
public class GenderAdapter extends ArrayAdapter<String>{
    private Context mContext;
    private int RESOURCE;
    private List<String> mGenderList;

    public GenderAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        mContext = context;
        RESOURCE = resource;
        mGenderList = objects;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder vh = null;

        if(view == null) {
            LayoutInflater inflater =
                    (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(RESOURCE, parent, false);
            vh = new ViewHolder();
            vh.txtGender = (CustomTextView)view.findViewById(R.id.txtGender);
            vh.relMain = (RelativeLayout)view.findViewById(R.id.relMain);
            view.setTag(vh);
        }else {
            vh = (ViewHolder)view.getTag();
        }

        // If this is the initial dummy entry, make it hidden
        /*if (position == 0) {
            vh.txtGender.setHeight(0);
            vh.relMain.setMinimumHeight(0);
            vh.relMain.setVisibility(View.GONE);
        }else {
            // Pass convertView as null to prevent reuse of special case views
            vh.relMain.setVisibility(View.VISIBLE);
        }*/

        String gender = mGenderList.get(position);
        vh.txtGender.setText(gender);

        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder vh = null;

        if(view == null) {
            LayoutInflater inflater =
                    (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(RESOURCE, parent, false);
            vh = new ViewHolder();
            vh.txtGender = (CustomTextView)view.findViewById(R.id.txtGender);
            vh.relMain = (RelativeLayout)view.findViewById(R.id.relMain);
            view.setTag(vh);
        }else {
            vh = (ViewHolder)view.getTag();
        }

        String gender = mGenderList.get(position);
        vh.txtGender.setText(gender);

        return view;
    }

    static class ViewHolder {
        RelativeLayout relMain;
        CustomTextView txtGender;
    }
}
