package com.grood.in.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.grood.in.R;
import com.grood.in.customviews.CustomTextView;
import com.grood.in.interfaces.ICartListener;
import com.grood.in.models.CartBean;
import com.grood.in.util.CommonUtilities;
import com.grood.in.util.ItemCacheUtilities;

import java.util.List;

/**
 * Created by USER on 16-08-2015.
 */
public class CartAdapter extends ArrayAdapter<CartBean>{
    private Context mContext;
    private int RESOURCE;
    private List<CartBean> mList;
    private ICartListener mCartListener;
    private UpdatePopupListener mUpdateListener;

    public interface UpdatePopupListener {
        void onPopupUpdate();
    }

    public CartAdapter(Context context, int resource, List<CartBean> objects,
                       ICartListener listener, UpdatePopupListener updateListener) {
        super(context, resource, objects);
        mList = objects;
        mContext = context;
        RESOURCE = resource;
        mCartListener = listener;
        mUpdateListener  = updateListener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder vh = null;
        if(view == null){
            LayoutInflater inflater =
                    (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(RESOURCE,parent,false);
            vh = new ViewHolder();
            vh.txtPrice = (CustomTextView)view.findViewById(R.id.txtPrice);
            vh.txtProductName = (CustomTextView)view.findViewById(R.id.txtProductName);
            vh.txtQty = (CustomTextView)view.findViewById(R.id.txtQuantity);
            vh.imgDecrease = (ImageView)view.findViewById(R.id.btnDecrease);
            vh.imgIncrease = (ImageView)view.findViewById(R.id.btnIncrease);
            vh.imgDeleteItem = (ImageView)view.findViewById(R.id.imgDelete);
            vh.imgProduct =  (ImageView)view.findViewById(R.id.imgProduct);
            view.setTag(vh);
        }else {
            vh = (ViewHolder)view.getTag();
        }

        CartBean cartObj = mList.get(position);
        vh.imgDecrease.setTag(String.valueOf(position));
        vh.imgIncrease.setTag(String.valueOf(position));
        vh.imgDeleteItem.setTag(String.valueOf(position));

        vh.txtQty.setText("" + cartObj.getQuantity());
        vh.txtPrice.setText(" र " + cartObj.getPrice());
        vh.txtProductName.setText(cartObj.getProductName());

        vh.imgDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView view = (ImageView)v;
                CartBean cartObj = mList.get(Integer.parseInt((String)view.getTag()));
                if(mCartListener != null) {
                    mCartListener.onItemQtyDecreased((CartBean)CommonUtilities.deepClone(cartObj));
                }

                double qty  = cartObj.getQuantity();
                double price = cartObj.getPrice();
                double totalPrice;
                qty -= 1;

                if(qty <= 0) {
                    // do nothing
                    mList.remove(cartObj);
                }else {
                    totalPrice = price * qty;
                    cartObj.setQuantity(qty);
                    cartObj.setTotalPrice(totalPrice);
                    mList.set(Integer.parseInt((String) view.getTag()),cartObj);
                }
                notifyDataSetChanged();

                if(mUpdateListener != null) {
                    mUpdateListener.onPopupUpdate();
                }
            }
        });

        vh.imgIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView view = (ImageView)v;
                CartBean cartObj = mList.get(Integer.parseInt((String)view.getTag()));
                if(mCartListener != null) {
                    mCartListener.onItemQtyIncreased((CartBean) CommonUtilities.deepClone(cartObj));
                }

                double qty  = cartObj.getQuantity();
                double price = cartObj.getPrice();
                double totalPrice;
                qty += 1;

                totalPrice = price * qty;
                cartObj.setQuantity(qty);
                cartObj.setTotalPrice(totalPrice);
                mList.set(Integer.parseInt((String) view.getTag()),cartObj);

                notifyDataSetChanged();

                if(mUpdateListener != null) {
                    mUpdateListener.onPopupUpdate();
                }
            }
        });

        vh.imgDeleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView view = (ImageView)v;
                CartBean cartObj = mList.get(Integer.parseInt((String)view.getTag()));
                if(mCartListener != null) {
                    mCartListener.onItemDeleted((CartBean)CommonUtilities.deepClone(cartObj));
                }
                mList.remove(cartObj);
                ItemCacheUtilities.deleteFromCart(cartObj);
                notifyDataSetChanged();

                if(mUpdateListener != null) {
                    mUpdateListener.onPopupUpdate();
                }
            }
        });

        CommonUtilities.getAQInstance()
                .id(vh.imgProduct)
                .image(cartObj.getProductImage(), true, true, 200,0);

        return view;
    }

    static class ViewHolder {
        CustomTextView txtProductName;
        CustomTextView txtPrice;
        CustomTextView txtQty;
        ImageView imgDecrease;
        ImageView imgIncrease;
        ImageView imgProduct;
        ImageView imgDeleteItem;
    }
}
