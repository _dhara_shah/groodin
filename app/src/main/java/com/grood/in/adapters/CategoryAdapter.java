package com.grood.in.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import com.facebook.FacebookRequestError;
import com.grood.in.R;
import com.grood.in.customviews.CustomEditText;
import com.grood.in.customviews.CustomTextView;
import com.grood.in.models.Category;

import java.util.List;

/**
 * Created by USER on 09-08-2015.
 */
public class CategoryAdapter extends BaseExpandableListAdapter{
    private Context mContext;
    private List<Category> mCategoryList;

    public CategoryAdapter(Context context, List<Category> categoryList) {
        mContext = context;
        mCategoryList = categoryList;
    }

    @Override
    public int getGroupCount() {
        return mCategoryList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mCategoryList.get(groupPosition).getSubcategory().size();
    }

    @Override
    public Category getGroup(int groupPosition) {
        return mCategoryList.get(groupPosition);
    }

    @Override
    public Category getChild(int groupPosition, int childPosition) {
        return mCategoryList.get(groupPosition).getSubcategory().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder vh = null;
        if(view == null) {
            LayoutInflater inflater =
                    (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.individual_parent, parent,false);
            vh  = new ViewHolder();
            vh.txtCategoryName = (CustomTextView)view.findViewById(R.id.txtCategoryName);
            view.setTag(vh);
        }else {
            vh = (ViewHolder)view.getTag();
        }
        vh.txtCategoryName.setText(getGroup(groupPosition).getName());
        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder vh = null;
        if(view == null) {
            LayoutInflater inflater =
                    (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.individual_child, parent,false);
            vh  = new ViewHolder();
            vh.txtSubCatName = (CustomTextView)view.findViewById(R.id.txtSubcategoryName);
            view.setTag(vh);
        }else {
            vh = (ViewHolder)view.getTag();
        }
        vh.txtSubCatName.setText(getChild(groupPosition,childPosition).getName());
        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    static class ViewHolder {
        CustomTextView txtCategoryName;
        CustomTextView txtSubCatName;
    }
}
