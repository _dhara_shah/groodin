package com.grood.in.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.grood.in.R;
import com.grood.in.customviews.CustomTextView;
import com.grood.in.interfaces.ICartListener;
import com.grood.in.models.CartBean;
import com.grood.in.util.CommonUtilities;
import com.grood.in.util.ItemCacheUtilities;
import com.grood.in.util.constant.Global;

import java.util.List;

/**
 * Created by USER on 21-08-2015.
 */
public class CheckoutAdapter extends ArrayAdapter<CartBean>{
    private Context  mContext;
    private int RESOURCE;
    private ICartListener mCartListener;
    private List<CartBean> cartBeanList;

    public CheckoutAdapter(Context context, int resource,
                           List<CartBean> objects, ICartListener cartListener) {
        super(context, resource, objects);
        mContext = context;
        RESOURCE = resource;
        cartBeanList = objects;
        mCartListener = cartListener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view=  convertView;
        ViewHolder vh = null;
        if(view== null) {
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.individual_checkout, parent, false);
            vh = new ViewHolder();
            vh.txtTotalPrice = (CustomTextView) view.findViewById(R.id.txtPrice);
            vh.txtProductName = (CustomTextView)view.findViewById(R.id.txtProductName);
            vh.imgDelete = (ImageView)view.findViewById(R.id.imgDelete);
            vh.imgProduct  = (ImageView)view.findViewById(R.id.imgProduct);
            view.setTag(vh);
        }else {
            vh = (ViewHolder)view.getTag();
        }

        CartBean cartBean = cartBeanList.get(position);
        vh.txtProductName.setText(cartBean.getProductName());

        double totalPrice = cartBean.getQuantity() * cartBean.getPrice();
        vh.txtTotalPrice.setText(" र "+ totalPrice);

        vh.imgDelete.setTag(String.valueOf(position));
        vh.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView img = (ImageView) v;
                int position = Integer.parseInt((String) img.getTag());
                CartBean obj = cartBeanList.get(position);
                if (mCartListener != null) {
                    mCartListener.onItemDeleted((CartBean) CommonUtilities.deepClone(obj));
                }
                cartBeanList.remove(obj);
                ItemCacheUtilities.deleteFromCart(obj);

                Intent intent = new Intent(Global.BROADCAST_DELETE_ITEM);
                mContext.sendBroadcast(intent);

                notifyDataSetChanged();
            }
        });

        CommonUtilities.getAQInstance()
                .id(vh.imgProduct)
                .image("https://" + cartBean.getProductImage(), true,true);

        return view;
    }

    static class ViewHolder {
        CustomTextView txtProductName;
        ImageView imgDelete;
        ImageView imgProduct;
        CustomTextView txtTotalPrice;
    }
}
