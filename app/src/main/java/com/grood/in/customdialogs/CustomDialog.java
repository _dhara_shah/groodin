package com.grood.in.customdialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.grood.in.GroodApp;
import com.grood.in.R;
import com.grood.in.activities.ConfirmOrderActivity_;
import com.grood.in.activities.LoginActivity_;
import com.grood.in.adapters.CartAdapter;
import com.grood.in.customviews.CustomButton;
import com.grood.in.customviews.CustomTextView;
import com.grood.in.interfaces.ICartListener;
import com.grood.in.models.Product;
import com.grood.in.models.UserAddress;
import com.grood.in.prefs.SharedPrefs;
import com.grood.in.util.CommonUtilities;
import com.grood.in.util.ItemCacheUtilities;
import com.grood.in.util.constant.Global;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by USER on 08-08-2015.
 */
public class CustomDialog {
    private static boolean isError;
    public interface DismissListener {
        void onDismissed();
    }

    public interface AddressListener{
        void onAddressListener();
    }

    public interface PopupMenuListener {
        void  onPopupMenuClicked(int menuId, Product product);
    }

    public static Dialog showProgressDialog(Context context) {
        Dialog dialog = new Dialog(context,
                android.R.style.Theme_Translucent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_dialog_progress, null);
        dialog.setContentView(view);

        ProgressWheel wheel = (ProgressWheel) view.findViewById(R.id.progressWheel);
        wheel.spin();

        Window window = dialog.getWindow();
        window.clearFlags(WindowManager
                .LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawableResource(R.drawable.dialogbackground);
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        return dialog;
    }

    public static void showAlertDialog(Context context,String message){
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }

    public static void showAlertDialog(Context context,String message, final DismissListener listener){
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if(listener != null) {listener.onDismissed();}
                    }
                })
                .create()
                .show();
    }

    public static void showPopupMenu(final Context context, View view,
                                     final Product product,
                                     final PopupMenuListener listener) {
        PopupMenu popup = new PopupMenu(context, view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.menu_items, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                Toast.makeText(context,
                        "You Clicked : " + item.getTitle(),
                        Toast.LENGTH_SHORT).show();
                if(listener  != null) {listener.onPopupMenuClicked(item.getItemId(), product);}
                return true;
            }
        });
        popup.show();
    }

    public static PopupWindow showQuickCheckout(final Context context, View view,final ICartListener listener){
        LayoutInflater layoutInflater =
                (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.layout_quick_checkout, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());

        final CustomTextView txtTotalPrice = (CustomTextView)popupView.findViewById(R.id.txtTotalPrice);
        ListView lstView = (ListView)popupView.findViewById(R.id.cart_list);

        View viewFooter = layoutInflater.inflate(R.layout.layout_footer, null);
        lstView.addFooterView(viewFooter);

        CartAdapter adapter  = new CartAdapter(context,
                R.layout.individual_quick_cart,
                ItemCacheUtilities.getItemList(),
                listener,
                new CartAdapter.UpdatePopupListener() {
                    @Override
                    public void onPopupUpdate() {
                        txtTotalPrice.setText(" र " + ItemCacheUtilities.getTotalPrice());
                    }
                });
        lstView.setAdapter(adapter);

        txtTotalPrice.setText(" र " + ItemCacheUtilities.getTotalPrice());

        ImageView imgClosePopup = (ImageView)popupView.findViewById(R.id.imgClosePopup);
        imgClosePopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        CustomButton btnCheckout = (CustomButton)popupView.findViewById(R.id.btnCheckout);
        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                Intent intent = new Intent(GroodApp.getAppContext(),
                        ConfirmOrderActivity_.class);
                context.startActivity(intent);
                ((FragmentActivity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        popupWindow.showAsDropDown(view);
        return popupWindow;
    }

    public void showAddressPopup(final Context context,UserAddress userAddress,boolean isNew,final AddressListener listener){
        Dialog dialog = new Dialog(context,
                android.R.style.Theme_Translucent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_address, null);
        dialog.setContentView(view);

        Window window = dialog.getWindow();
        window.clearFlags(WindowManager
                .LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawableResource(R.drawable.dialogbackground);
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);

        final CustomTextView editAddressOne=(CustomTextView)view.findViewById(R.id.editAddressOne);
        final CustomTextView editAddressTwo=(CustomTextView)view.findViewById(R.id.editAddressTwo);
        final CustomTextView editName=(CustomTextView)view.findViewById(R.id.editName);
        final CustomTextView editCity=(CustomTextView)view.findViewById(R.id.editCity);
        final CustomTextView editCountry=(CustomTextView)view.findViewById(R.id.editCountry);
        final CustomTextView editZipcode=(CustomTextView)view.findViewById(R.id.editZipcode);
        CustomButton btnAddUpdate=(CustomButton)view.findViewById(R.id.btnAddUpdate);

        if(isNew){
            btnAddUpdate.setText(GroodApp.getAppContext().getResources().getString(R.string.add_new_address));
        }else{
            btnAddUpdate.setText(GroodApp.getAppContext().getResources().getString(R.string.update));
        }

        btnAddUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(editName.getText().toString().trim()) ||
                        TextUtils.isEmpty(editCity.getText().toString().trim()) ||
                        TextUtils.isEmpty(editCountry.getText().toString().trim()) ||
                        TextUtils.isEmpty(editZipcode.getText().toString())){
                    isError=true;
                    CustomDialog.showAlertDialog(context,
                            GroodApp.getAppContext().getResources().getString(R.string.enter_in_all_the_details_marked_star));
                    return;
                }

                if(TextUtils.isEmpty(editAddressOne.getText().toString().trim())){
                    CustomDialog.showAlertDialog(context,
                            GroodApp.getAppContext().getResources().getString(R.string.enter_in_all_the_details_marked_star));
                    return;
                }


            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (!isError) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }
}
