package com.grood.in.customdialogs;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

/**
 * Created by USER on 09-08-2015.
 */
public class DatePickerDialogFragment extends DialogFragment {
    private static OnDateSetListener mDateSetListener;
    private static DatePickerDialogFragment mFragment;
    public DatePickerDialogFragment() {
        // nothing to see here, move along
    }

    public void setListener(OnDateSetListener callback)  {
        mDateSetListener = (OnDateSetListener)callback;
    }

    public static DatePickerDialogFragment newInstance(OnDateSetListener callback) {
        mDateSetListener = (OnDateSetListener) callback;
        mFragment = new DatePickerDialogFragment();
        return mFragment;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar cal = Calendar.getInstance();
        return new DatePickerDialog(getActivity(),
                mDateSetListener, cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
    }
}
