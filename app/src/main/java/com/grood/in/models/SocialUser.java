package com.grood.in.models;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 19-08-2015.
 */
@Getter @Setter
public class SocialUser {
    private String userId;
    private String userToken;
    private String firstName;
    private String lastName;
    private String gender;
    private String email;
    private String birthdate;
    private String socialLoginType;
}
