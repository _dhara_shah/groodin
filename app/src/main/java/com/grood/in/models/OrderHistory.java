package com.grood.in.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 23-08-2015.
 */
@Getter @Setter
public class OrderHistory {
    private String reference;
    private String payment;
    @SerializedName("total_paid")
    private String totalPaid;
    @SerializedName("order_state")
    private String orderState;
    @SerializedName("date_added")
    private String orderDate;
}
