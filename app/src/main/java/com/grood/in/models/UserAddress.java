package com.grood.in.models;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 12-09-2015.
 */
@Getter @Setter
public class UserAddress {
    private String userName;
    private String addressOne;
    private String addressTwo;
    private String city;
    private String country;
    private String zipcode;
}
