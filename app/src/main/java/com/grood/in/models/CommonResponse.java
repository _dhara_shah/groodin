package com.grood.in.models;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 08-08-2015.
 */
@Getter @Setter
public class CommonResponse {
    private String status;
    private String msg;
}
