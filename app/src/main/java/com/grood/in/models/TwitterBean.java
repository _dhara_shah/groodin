package com.grood.in.models;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.grood.in.GroodApp;
import com.grood.in.R;
import com.grood.in.activities.BaseActivity;
import com.grood.in.fragments.LoginFragment_;
import com.grood.in.fragments.ProductDetailFragment_;
import com.grood.in.interfaces.GroodTwitterApiClient;
import com.grood.in.util.constant.Global;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.core.services.StatusesService;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.io.File;

import lombok.Getter;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.UploadedMedia;

/**
 * Created by USER on 29-07-2015.
 */
@EBean
public class TwitterBean {
    @RootContext
    BaseActivity baseActivity;

    @Getter
    TwitterAuthClient twitterAuthClient = new TwitterAuthClient();

    Fragment mFragment;

    public void connectTwitter(Fragment fragment) {
        mFragment = fragment;
        twitterAuthClient.authorize(mFragment.getActivity(), new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> twitterSessionResult) {
                //Session that includes everything
                TwitterSession twitterSession = twitterSessionResult.data;
                String userName = twitterSession.getUserName();
                long userId = twitterSession.getUserId();
                getProfileInformation(userName, userId);
            }

            @Override
            public void failure(TwitterException e) {
                // FIXME: I need to be fixed
                // Twitter returns a failure because the kit has not been updated to match the latest version of Retrofit
                // reference: https://twittercommunity.com/t/failed-to-get-request-token-method-post-must-have-a-request-body/38947
                // to fix this the gradle version of retrofit has been reverted back to 2.3.0 from 2.4.0
                Toast.makeText(baseActivity,
                        baseActivity.getString(R.string.twitter_authorization_failed),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getProfileInformation(String userName,  long userId) {
        GroodTwitterApiClient twitterApiClient =
                new GroodTwitterApiClient(TwitterCore.getInstance().getSessionManager().getActiveSession());
        twitterApiClient.getUserDetails().show(userId, new Callback<User>() {
            @Override
            public void success(Result<User> result) {
                getProfileData(result.data);
            }

            @Override
            public void failure(TwitterException e) {
                Toast.makeText(baseActivity.getApplicationContext(),
                        baseActivity.getString(R.string.twitter_profile_failure),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getProfileData(User data) {
        // send data to the webservice from here
        SocialUser socialUser = new SocialUser();
        socialUser.setUserId(String.valueOf(data.getId()));
        socialUser.setEmail(data.email);
        socialUser.setUserToken(String.valueOf(data.getId()));
        socialUser.setFirstName(data.name);
        socialUser.setSocialLoginType(Global.TWITTER);

        if(mFragment instanceof LoginFragment_){
            ((LoginFragment_)mFragment).socialLoginData(socialUser);
        }
    }

    public void disconnect() {
        TwitterCore.getInstance().getAppSessionManager().clearActiveSession();
    }

    public void shareOnTwitter(Product product, String filePath,  Fragment fragment) {
        mFragment = fragment;
        File file = new File(filePath);
        /*TweetComposer.Builder builder = new TweetComposer.Builder(GroodApp.getAppContext())
                .text("Buy " +
                        product.getProductName() + " @ " + product.getProductPrice() +
                        ". Visit https://grood.in for more information.")
                .image(Uri.fromFile(file));
        builder.show();*/

        try {
            Twitter twitter = new TwitterFactory().getInstance();
            UploadedMedia media = twitter.uploadMedia(file);

            StatusUpdate update = new StatusUpdate("Buy " +
                    product.getProductName() + " @ " + product.getProductPrice() +
                    ". Visit https://grood.in for more information.");
            update.setMediaIds(new long[]{media.getMediaId()});
            Status status = twitter.updateStatus(update);

            if(mFragment != null && mFragment instanceof ProductDetailFragment_){
                ((ProductDetailFragment_)mFragment).shareProduct(Global.TWITTER);
            }
        }catch (twitter4j.TwitterException e){
            e.printStackTrace();
        }
    }
}
