package com.grood.in.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 23-08-2015.
 */
@Getter @Setter
public class Address {
    @SerializedName("id_address")
    private String addressId;
    @SerializedName("firstname")
    private String firstName;
    @SerializedName("lastname")
    private String lastName;
    @SerializedName("address1")
    private String address1;
    @SerializedName("address2")
    private String address2;
    @SerializedName("postcode")
    private String postcode;;
    @SerializedName("city")
    private String city;
    @SerializedName("phone_mobile")
    private String mobileNumber;
}
