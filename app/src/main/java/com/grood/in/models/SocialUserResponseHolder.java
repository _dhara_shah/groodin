package com.grood.in.models;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 24-08-2015.
 */
@Getter @Setter
public class SocialUserResponseHolder {
    private SocialUserResponse response;
}
