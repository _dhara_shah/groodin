package com.grood.in.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 16-08-2015.
 */
@Getter @Setter
public class CartBean implements Serializable{
    private String productId;
    private String productName;
    private String productImage;
    private double price;
    private double quantity;
    private double totalPrice;
}
