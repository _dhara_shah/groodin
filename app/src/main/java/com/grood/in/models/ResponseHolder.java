package com.grood.in.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by USER on 08-08-2015.
 */
public class ResponseHolder extends CommonResponse{
    @SerializedName("data")
    private CustomerHolder customerHolder;

    public CustomerHolder getCustomerHolder() {
        return customerHolder;
    }

    public void setCustomerHolder(CustomerHolder customerHolder) {
        this.customerHolder = customerHolder;
    }
}
