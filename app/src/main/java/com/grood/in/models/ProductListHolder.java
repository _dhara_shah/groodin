package com.grood.in.models;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 10-08-2015.
 */
@Getter @Setter
public class ProductListHolder {
    private ProductList response;
}
