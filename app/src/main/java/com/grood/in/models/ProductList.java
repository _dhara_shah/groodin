package com.grood.in.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 10-08-2015.
 */
@Getter @Setter
public class ProductList extends CommonResponse{
    @SerializedName("data")
    private List<Product> productList;
    @SerializedName("total_products")
    private String totalCount;
}
