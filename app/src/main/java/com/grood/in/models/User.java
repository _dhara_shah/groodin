package com.grood.in.models;

import org.androidannotations.annotations.EBean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by USER on 08-08-2015.
 */
@Getter @Setter
public class User {
    private String birthday;
    private String firstName;
    private String lastName;
    private String genderId;
    private String email;
    private String password;
    private String newPassword;
}
