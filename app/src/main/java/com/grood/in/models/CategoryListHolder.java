package com.grood.in.models;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 09-08-2015.
 */
@Getter @Setter
public class CategoryListHolder {
    private CategoryList response;
}
