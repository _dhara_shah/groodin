package com.grood.in.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 08-08-2015.
 */
@Getter  @Setter
public class Customer {
    private String id;
    @SerializedName("lastname")
    private String lastName;
    @SerializedName("firstname")
    private String firstName;
    @SerializedName("id_gender")
    private String genderId;
}
