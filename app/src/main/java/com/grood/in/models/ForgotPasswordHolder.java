package com.grood.in.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 09-08-2015.
 */
@Getter @Setter
public class ForgotPasswordHolder {
    private CommonResponse response;
}
