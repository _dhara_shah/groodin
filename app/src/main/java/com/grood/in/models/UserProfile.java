package com.grood.in.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 23-08-2015.
 */
@Getter @Setter
public class UserProfile {
    @SerializedName("customer_id")
    private String customerId;
    @SerializedName("fristname")
    private String firstName;
    @SerializedName("lastname")
    private String lastName;
    @SerializedName("id_gender")
    private String gender;
    @SerializedName("birthday")
    private String birthday;
    @SerializedName("email")
    private String email;
    @SerializedName("addessses")
    private List<Address> addressList;
    @SerializedName("order_history")
    private List<OrderHistory> orderHistoryList;
}
