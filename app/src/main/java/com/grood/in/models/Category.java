package com.grood.in.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 09-08-2015.
 */
@Getter @Setter
public class Category implements Serializable{
    @SerializedName("id_category")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("subcategory")
    private List<Category> subcategory;
    @SerializedName("subsubcategory")
    private List<Category> subSubCategory;
}
