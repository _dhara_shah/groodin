package com.grood.in.models;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.grood.in.GroodApp;
import com.grood.in.R;
import com.grood.in.activities.BaseActivity;
import com.grood.in.fragments.LoginFragment_;
import com.grood.in.fragments.ProductDetailFragment_;
import com.grood.in.util.constant.Global;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.json.JSONObject;

import java.util.Arrays;

import lombok.Getter;

/**
 * Created by USER on 20-07-2015.
 */
@EBean
public class FacebookBean extends ProfileTracker{
    @RootContext
    BaseActivity baseActivity;

    Fragment mFragment;

    ShareDialog shareDialog;

    @Getter
    final CallbackManager callbackManager = CallbackManager.Factory.create();

    @Override
    protected void onCurrentProfileChanged(Profile profile, Profile newProfile) {
        // get user details here
        Log.e("dhara","userId >>> " + newProfile.getId());
        Log.e("dhara","name >>> " + newProfile.getName());
        Log.e("dhara","firstName >>> "  + newProfile.getFirstName());
        Log.e("dhara","lastName >>> " + newProfile.getLastName());

        // send to webservice
        final SocialUser socialUser = new SocialUser();
        socialUser.setFirstName((newProfile.getFirstName() != null) ? newProfile.getFirstName() : newProfile.getName());
        socialUser.setLastName((newProfile.getLastName() != null) ? newProfile.getLastName() : "");
        socialUser.setUserToken(newProfile.getId());
        socialUser.setUserId(newProfile.getId());
        socialUser.setBirthdate("");
        socialUser.setSocialLoginType(Global.FACEBOOK);

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        GraphRequest profileGraphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                Log.e("graph response", response.toString());
                try{
                    if(object.getString("gender").equals("male")){
                        socialUser.setGender("1");
                    }else {
                        socialUser.setGender("2");
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }

                if(mFragment instanceof LoginFragment_){
                    ((LoginFragment_)mFragment).socialLoginData(socialUser);
                }
            }
        });

        Bundle requestParameter = new Bundle();
        requestParameter.putString("fields", "id,birthday,first_name,gender,last_name,link");
        profileGraphRequest.setParameters(requestParameter);
        profileGraphRequest.executeAsync();


    }

    public void connectFacebook(Fragment fragment) {
        mFragment = fragment;
        LoginManager.getInstance().logInWithReadPermissions(baseActivity,
                Arrays.asList("public_profile", "email"));
    }

    public void shareOnFacebook(Product product, Fragment fragment) {
        mFragment = fragment;
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent content = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse("https://grood.in"))
                    .setContentDescription(product.getProductName() + " @ " + product.getProductPrice())
                    .setContentTitle(GroodApp.getAppContext().getString(R.string.app_name))
                    .setImageUrl(Uri.parse(product.getProductImage()))
                    .build();
            shareDialog.show(content);
        }
    }

    public void onResult(int requestCode, int resultCode, Intent data){
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

    public void registerShareDialogCallback(Fragment fragment) {
        mFragment = fragment;
        shareDialog = new ShareDialog(mFragment);

        // this part is optional
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                // successful so call the share async task
                Toast.makeText(GroodApp.getAppContext(),
                        "Share success!",
                        Toast.LENGTH_SHORT).show();

                if(mFragment != null) {
                    if(mFragment instanceof ProductDetailFragment_) {
                        ((ProductDetailFragment_)mFragment).shareProduct(Global.FACEBOOK);
                    }
                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }
}
