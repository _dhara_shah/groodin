package com.grood.in.models;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 23-08-2015.
 */
@Getter @Setter
public class ShareResponse extends CommonResponse{
    private ShareProductData data;
}
