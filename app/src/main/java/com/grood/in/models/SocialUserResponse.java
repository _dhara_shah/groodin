package com.grood.in.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 24-08-2015.
 */
@Getter @Setter
public class SocialUserResponse extends CommonResponse{
    @SerializedName("data")
    private SocialUserBean socialUserBean;
}
