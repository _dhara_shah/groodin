package com.grood.in.models;

import com.google.gson.annotations.SerializedName;
import com.grood.in.fragments.ProfileFragment;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 23-08-2015.
 */
@Getter @Setter
public class ProfileResponseHolder {
    @SerializedName("response")
    private ProfileResponse profileResponse;
}
