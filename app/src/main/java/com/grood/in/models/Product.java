package com.grood.in.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 10-08-2015.
 */
@Getter @Setter
public class Product implements Serializable{
    @SerializedName("id_product")
    private String productId;
    @SerializedName("image")
    private String productImage;
    @SerializedName("name")
    private String productName;
    @SerializedName("description")
    private String productDesc;
    @SerializedName("price")
    private double productPrice;
    @SerializedName("quantity")
    private String productQuantity;
    @SerializedName("condition")
    private String productCondition;
    @SerializedName("fb_count")
    private String facebookShareCount;
    @SerializedName("twitter_count")
    private String twitterShareCount;
    @SerializedName("pintrest_count")
    private String pinterestShareCount;
}
