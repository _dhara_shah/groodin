package com.grood.in.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 23-08-2015.
 */
@Getter @Setter
public class ShareProductData {
    @SerializedName("twitter_count")
    private String twitterShareCount;
    @SerializedName("fb_count")
    private String fbShareCount;
    @SerializedName("pintrest_count")
    private String pinterestShareCount;
}
