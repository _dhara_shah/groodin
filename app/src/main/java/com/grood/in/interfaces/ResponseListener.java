package com.grood.in.interfaces;

/**
 * Created by USER on 08-08-2015.
 */
public interface ResponseListener  {
    void onResponseReceived(Object object);
}
