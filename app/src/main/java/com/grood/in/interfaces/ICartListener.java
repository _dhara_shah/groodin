package com.grood.in.interfaces;

import com.grood.in.models.CartBean;

/**
 * Created by USER on 16-08-2015.
 */
public interface ICartListener {
    void onItemQtyIncreased(CartBean cartBean);
    void onItemQtyDecreased(CartBean cartBean);
    void onItemDeleted(CartBean cartBean);
}
