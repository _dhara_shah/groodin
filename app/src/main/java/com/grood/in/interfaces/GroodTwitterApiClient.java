package com.grood.in.interfaces;

import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterSession;

/**
 * Created by USER on 29-07-2015.
 */
public class GroodTwitterApiClient extends TwitterApiClient{
    public GroodTwitterApiClient(TwitterSession session) {
        super(session);
    }

    /**
     * Provide CustomService with defined endpoints
     */
    public GroodTwitterApi getUserDetails() {
        return getService(GroodTwitterApi.class);
    }
}
