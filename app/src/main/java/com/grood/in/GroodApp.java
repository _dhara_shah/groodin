package com.grood.in;

import android.app.Application;
import android.content.Context;

import com.facebook.FacebookSdk;
import com.grood.in.util.network.NetworkUtilities;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import org.androidannotations.annotations.EApplication;

import io.fabric.sdk.android.Fabric;

/**
 * Created by USER on 19-07-2015.
 */

@EApplication
public class GroodApp extends Application{
    private static GroodApp mApp;
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mApp = this;
        mContext = this;
        setupNetwork();
        setupFacebook();
        setupTwitter();
    }

    private void setupNetwork(){
        NetworkUtilities.init();
    }

    private void setupFacebook() {
        FacebookSdk.sdkInitialize(this);
    }

    private void setupTwitter() {
        TwitterAuthConfig authConfig =
                new TwitterAuthConfig(getString(R.string.twitter_consumer_key),
                        getString(R.string.twitter_consumer_secret));
        Fabric.with(this, new TwitterCore(authConfig),new TweetComposer());
    }

    public static GroodApp getAppContext(){
        if(mApp == null){
            mApp = (GroodApp)mContext;
        }
        return mApp;
    }
}
