package com.grood.in.util.search;

import com.google.common.base.Predicate;
import com.grood.in.models.CartBean;

/**
 * Created by USER on 24-08-2015.
 */
public class CartPredicate implements Predicate<CartBean>{
    private CartBean mCartBean;
    public CartPredicate(CartBean cartBean) {
        mCartBean = cartBean;
    }
    @Override
    public boolean apply(CartBean cartBean) {
        if(mCartBean.getProductId().equals(cartBean.getProductId()) &&
                mCartBean.getQuantity() == cartBean.getQuantity())  {
            return true;
        }
        return false;
    }
}
