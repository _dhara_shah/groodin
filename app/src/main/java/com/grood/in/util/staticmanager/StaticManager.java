package com.grood.in.util.staticmanager;

import com.grood.in.models.Category;
import com.grood.in.models.CategoryList;
import com.grood.in.models.CategoryListHolder;
import com.grood.in.models.Product;
import com.grood.in.models.ProductList;
import com.grood.in.models.ProductListHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USER on 26-08-2015.
 */
public class StaticManager {
    public static CategoryListHolder getCategories(){
        CategoryListHolder holder = new CategoryListHolder();
        CategoryList categoryList = new CategoryList();
        List<Category> categories = new ArrayList<>();

        for(int i=0;i<2;i++) {
            Category category = new Category();
            category.setId("" + (i + 1));
            category.setName("Category " + (i + 1));
            List<Category> subcategories = new ArrayList<>();
            for(int j=0;j<2;j++){
                Category subCat = new Category();
                subCat.setId(""+(j+1+10));
                subCat.setName("Subcategory " + (j + 1));
                subcategories.add(subCat);
            }
            category.setSubcategory(subcategories);
            categories.add(category);
        }
        categoryList.setStatus("1");
        categoryList.setCategoryList(categories);
        holder.setResponse(categoryList);
        return  holder;
    }

    public static ProductListHolder getProducts(){
        ProductListHolder holder = new ProductListHolder();
        ProductList productList = new ProductList();
        List<Product> products = new ArrayList<>();

        for(int i=0;i<10;i++) {
           Product product  = new Product();
            product.setProductId("" + (i + 1 + 20));
            product.setProductName("Product item " + (i + 1));
            product.setFacebookShareCount("20");
            product.setTwitterShareCount("20");
            product.setPinterestShareCount("20");
            product.setProductCondition("New");
            product.setProductDesc("this is a dummy product...gnkggkngj fdgdfgkgnjd dfgnfgjdgjn dgnjkgndfjkgn " +
                    "ngkndjkngdkgnnkjngdngjkn njdgnjg dfdfdfgg dfdffsdf fdfdggfgg gfgdfgdfg dgdgdfg " +
                    "gnkggkngj fdgdfgkgnjd dfgnfgjdgjn dgnjkgndfjkgn " +
                    "gkndjkngdkgnnkjngdngjkn njdgnjg dfdfdfgg dfdffsdf fdfdggfgg gfgdfgdfg dgdgdfg");
            product.setProductPrice((i+1+20) * (80.50));
            products.add(product);
        }
        productList.setStatus("1");
        productList.setTotalCount("10");
        productList.setProductList(products);
        holder.setResponse(productList);
        return holder;
    }
}
