package com.grood.in.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.os.Environment;
import com.grood.in.GroodApp;
import com.grood.in.util.constant.Global;

import org.apache.http.impl.client.DefaultHttpClient;

public class FileUtilities {
	private static final String FILE_PATH = "/data/userdata/";
	private static final String FILE_NAME= "shopping_bag.txt";

	private static String FILE_IMAGE_FOLDER = Environment.getExternalStorageDirectory() +
			"/" +
			GroodApp.getAppContext().getPackageName() +
			"/productimages/";

	public static File downloadImage(String imageUrl) {
		try {
			File file = new File(FILE_IMAGE_FOLDER);
			if (!file.exists()) {
				file.mkdir();
				file.mkdirs();
			}
			File writingFile = null;
			writingFile = new File(file, "/grood_product_image.png");

			URL url = new URL("https://"+imageUrl);
			URLConnection connection = url.openConnection();
			connection.connect();

			HttpURLConnection con = null;
			try {
				URL obj = new URL("https://"+imageUrl);
				con = (HttpURLConnection) obj.openConnection();
				con.setRequestMethod("GET");
				con.setConnectTimeout(50000);

				if(con.getResponseCode() == HttpURLConnection.HTTP_OK) {
					InputStream is = con.getInputStream();
					FileOutputStream fileOutput = new FileOutputStream(writingFile);
					InputStream inputStream = con.getInputStream();

					byte[] buffer = new byte[1024];
					int bufferLength = 0;

					while ( (bufferLength = inputStream.read(buffer)) > 0 ) {
						fileOutput.write(buffer, 0, bufferLength);
					}
					fileOutput.close();
					return writingFile;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}finally {
				if(con != null) {
					con.disconnect();
				}
			}
			return null;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return null;
	}


	public static void writeObjectToFile(Object object) {
		try {
			File file = new File(Environment.getExternalStorageDirectory() + 
					"/" +
					GroodApp.getAppContext().getPackageName() +
					FILE_PATH);

			if(!file.exists()) {
				file.mkdir();
				file.mkdirs();
			}
			File writingFile = new File(file, FILE_NAME);
			if(!writingFile.exists())
				writingFile.createNewFile();

			FileOutputStream fout = new FileOutputStream(writingFile);
			ObjectOutputStream oos = new ObjectOutputStream(fout);   
			oos.writeObject(object);
			oos.close();
		}catch(IOException e) {
			e.printStackTrace();
			
			// there was an error writing to the file
			// so store in the cache
			CacheUtilities.storeDataIntoCache(object);
		}
	}
	
	public static Object readObjectFromFile(String whatToGet){
		try {
			File file = new File(Environment.getExternalStorageDirectory() + 
					"/" +
					GroodApp.getAppContext().getPackageName() +
					FILE_PATH);

			if(!file.exists()) {
				file.mkdir();
				file.mkdirs();
			}
			File readingFile = null;
			
			if(whatToGet.equals(Global.CACHE_DATA)) {
				readingFile = new File(file, FILE_NAME);
			}

			FileInputStream fin = new FileInputStream(readingFile);
			ObjectInputStream ois = new ObjectInputStream(fin);
			Object response = ois.readObject();
			ois.close();
			fin.close();
			return response;
		}catch(IOException e) {
			e.printStackTrace();
			
			// the file was not found so return data from the cache
			Object response = CacheUtilities.getDataFromCache(whatToGet);
			return response;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
}
