package com.grood.in.util.search;

import com.google.common.base.Predicate;
import com.grood.in.models.CartBean;

/**
 * Created by USER on 16-08-2015.
 */
public class ProductPredicate implements Predicate<CartBean>{
    private String mProductId;

    public ProductPredicate(String productId){
        mProductId = productId;
    }
    @Override
    public boolean apply(CartBean cartBean) {
        if(cartBean.getProductId().equals(mProductId)) {
            return true;
        }else {
            return false;
        }
    }
}
