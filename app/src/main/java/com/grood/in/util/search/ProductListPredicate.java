package com.grood.in.util.search;

import com.google.common.base.Predicate;
import com.grood.in.models.Product;

import java.util.Locale;

/**
 * Created by USER on 30-08-2015.
 */
public class ProductListPredicate implements Predicate<Product> {
    private String mSearchString;
    public ProductListPredicate(String searchString) {
        mSearchString = searchString;
    }

    @Override
    public boolean apply(Product product) {
        if (product.getProductName().toLowerCase(Locale.ENGLISH)
                .contains(mSearchString.toLowerCase(Locale.ENGLISH))) {
            return true;
        }
        return false;
    }
}
