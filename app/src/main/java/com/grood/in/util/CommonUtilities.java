package com.grood.in.util;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.grood.in.GroodApp;
import com.grood.in.adapters.CheckoutAdapter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class CommonUtilities {
    private static AQuery aQuery;

    public static int getScreenHeight(){
		return GroodApp.getAppContext().getResources().getDisplayMetrics().heightPixels;
	}

	public static int getScreenWidth() {
		return GroodApp.getAppContext().getResources().getDisplayMetrics().widthPixels;
	}

	public static int getOriginalScreenWidth(){
		return ((int)((float)GroodApp.getAppContext().getResources().getDisplayMetrics().widthPixels /
				GroodApp.getAppContext().getResources().getDisplayMetrics().density));
	}

	public static int getOriginalScreenHeight(){
		return ((int)((float)GroodApp.getAppContext().getResources().getDisplayMetrics().heightPixels /
				GroodApp.getAppContext().getResources().getDisplayMetrics().density));
	}

    public static AQuery getAQInstance() {
        if (aQuery == null) {
            aQuery = new AQuery(GroodApp.getAppContext());
        }
        return aQuery;
    }

    public static Object deepClone(Object obj) {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			// serialize and pass the object
			oos.writeObject(obj);
			oos.flush();
			ByteArrayInputStream bin =
					new ByteArrayInputStream(bos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bin);
			// return the new object
			return ois.readObject();
		}catch (IOException e){
			e.printStackTrace();
		}catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return obj;
	}

	public static void setListViewHeightBasedOnChildren(ListView listView) {
		CheckoutAdapter listAdapter = (CheckoutAdapter)listView.getAdapter();
		if (listAdapter == null) {
			// pre-condition
			return;
		}

		int totalHeight = 0;
		int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
			totalHeight += listItem.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

}
