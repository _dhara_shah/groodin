package com.grood.in.util.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.grood.in.GroodApp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by user on 30/07/2015.
 */
public class NetworkUtilities {
    private static HashMap<String, String> NETWORK_URLS;
    private static String BASE_URL = "https://grood.in/module/skeleton/index.php?fc=module&module=skeleton&";

    public static void init() {
        NETWORK_URLS = new HashMap<>();
        NETWORK_URLS.put(NETWORK_FUNCTIONS.SignUp.name(), BASE_URL + NETWORK_FUNCTIONS.SignUp.getMethodName());
        NETWORK_URLS.put(NETWORK_FUNCTIONS.Login.name(), BASE_URL + NETWORK_FUNCTIONS.Login.getMethodName());
        NETWORK_URLS.put(NETWORK_FUNCTIONS.GetProductList.name(), BASE_URL + NETWORK_FUNCTIONS.GetProductList.getMethodName());
        NETWORK_URLS.put(NETWORK_FUNCTIONS.ForgotPassword.name(), BASE_URL + NETWORK_FUNCTIONS.ForgotPassword.getMethodName());
        NETWORK_URLS.put(NETWORK_FUNCTIONS.GetCategories.name(), BASE_URL + NETWORK_FUNCTIONS.GetCategories.getMethodName());
        NETWORK_URLS.put(NETWORK_FUNCTIONS.GetTotalCount.name(), BASE_URL + NETWORK_FUNCTIONS.GetTotalCount.getMethodName());
        NETWORK_URLS.put(NETWORK_FUNCTIONS.SocialLogin.name(), BASE_URL + NETWORK_FUNCTIONS.SocialLogin.getMethodName());
        NETWORK_URLS.put(NETWORK_FUNCTIONS.GetProfile.name(), BASE_URL +  NETWORK_FUNCTIONS.GetProfile.getMethodName());
        NETWORK_URLS.put(NETWORK_FUNCTIONS.UpdateProfile.name(), BASE_URL +  NETWORK_FUNCTIONS.UpdateProfile.getMethodName());
    }

    public static String makeWebserviceCall(METHOD method,
                                            NETWORK_FUNCTIONS function,
                                            HashMap<String, String> params) {
        String response = "";
        String url = NETWORK_URLS.get(function.name());
        switch (method) {
            case GET:
                response = getResponseFromGet(url);
                break;

            case POST:
                response = postData(url, params);
                break;

            default:
                break;
        }
        return response;
    }

    /**
     * performs the post operation and returns the response
     *
     * @param url
     * @param params
     * @return
     */
    public static String postData(String url, HashMap<String, String> params) {
        HttpURLConnection conn = null;
        try {
            URL urlObj = new URL(url);
            conn = (HttpURLConnection) urlObj.openConnection();
            conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
            conn.setReadTimeout(50000);
            conn.setConnectTimeout(50000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(params));

            writer.flush();
            writer.close();
            os.close();
            int responseCode=conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String response = getResponseFromInputStream(conn.getInputStream());
                return response;
            }
        } catch(ProtocolException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            if(conn != null) {
                conn.disconnect();
            }
        }

        return null;
    }

    private static String getResponseFromGet(String url) {
        HttpURLConnection con = null;
        try {
            URL obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
            con.setConnectTimeout(50000);
            con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

            if(con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream is = con.getInputStream();
                String response = getResponseFromInputStream(is);
                return response;
            }
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
            if(con != null) {
                con.disconnect();
            }
        }
        return null;
    }

    /**
     * Checks if network is available
     *
     * @return
     */
    public static boolean isNetworkAvailable() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm =
                (ConnectivityManager) GroodApp.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    private static String getResponseFromInputStream(InputStream inputStream) {
        try {
            StringBuffer stringBuffer = new StringBuffer();
            String line;
            BufferedReader br=new BufferedReader(new InputStreamReader(inputStream));
            while ((line=br.readLine()) != null) {
                stringBuffer.append(line);
            }
            return stringBuffer.toString();
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    private static String getPostDataString(HashMap<String, String> params) {
        StringBuilder result = new StringBuilder();
        try {
            boolean first = true;
            for(Map.Entry<String, String> entry : params.entrySet()){
                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
        }catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return result.toString();
    }

    public enum METHOD {
        GET,
        POST;
    }

    public enum NETWORK_FUNCTIONS {
        SignUp("controller=customer&action=create"),
        Login("controller=customer&action=login"),
        ForgotPassword("controller=customer&action=forgot"),
        GetCategories("controller=category&action=details"),
        GetProductList("controller=category&action=productlist"),
        GetTotalCount("controller=details&action=totalproduts"),
        SocialLogin("controller=customer&action=social_login"),
        GetProfile("controller=account&action=getprofile"),
        UpdateProfile("controller=account&action=myprofile");

        private String value;

        NETWORK_FUNCTIONS(String value) {
            this.value = value;
        }
        public String getMethodName() {
            return this.value;
        }
    }
}
