package com.grood.in.util;

import java.util.HashMap;
import java.util.List;

import com.grood.in.models.CartBean;
import com.grood.in.util.constant.Global;

import android.support.v4.util.LruCache;

public class CacheUtilities {
	private static LruCache<String, Object> lruCache;
	private static final int MAX_SIZE = 1024 * 1024 * 8;
	
	public static void init(){
		lruCache = new LruCache<>(MAX_SIZE);
	}
	
	private static LruCache<String, Object> getInstance(){
		if(lruCache == null) {
			init();
		}
		return lruCache;
	}
	
	public static void storeDataIntoCache(Object object) {
		lruCache = getInstance();
		lruCache.put(Global.CACHE_DATA, (List<CartBean>)object);
	}
	
	public static Object getDataFromCache(String whatToGet){
		lruCache = getInstance();
		if(whatToGet.equals(Global.CACHE_DATA)) {
			Object obj = lruCache.get(Global.CACHE_DATA);
			if(obj!= null && obj instanceof List<?>) {
				return (List<CartBean>)obj;
			}else {
				return null;
			}
		}else {
			return null;
		}
	}
}
