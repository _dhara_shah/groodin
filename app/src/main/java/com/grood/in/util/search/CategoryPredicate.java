package com.grood.in.util.search;

import com.google.common.base.Predicate;
import com.grood.in.models.Category;

import java.util.Locale;

/**
 * Created by USER on 30-08-2015.
 */
public class CategoryPredicate implements Predicate<Category>{
    private String mSearchString;
    public CategoryPredicate(String searchString) {
        mSearchString = searchString;
    }

    @Override
    public boolean apply(Category category) {
        for(Category catObj : category.getSubcategory()) {
            if(catObj.getName().toLowerCase(Locale.ENGLISH)
                    .contains(mSearchString.toLowerCase(Locale.ENGLISH))) {
                return true;
            }
        }
        return false;
    }
}
