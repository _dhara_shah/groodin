package com.grood.in.util.constant;

/**
 * Created by USER on 08-08-2015.
 */
public class Global {
    public static final String NETWORK_FIRSTNAME="firstname";
    public static final String NETWORK_LASTNAME="lastname";
    public static final String NETWORK_BIRTHDAY="birthday";
    public static final String NETWORK_ID_GENDER="id_gender";
    public static final String NETWORK_GENDER="gender";
    public static final String NETWORK_EMAIL="email";
    public static final String NETWORK_PASSWORD="password";
    public static final String NETWORK_PASSWD="passwd";
    public static final String NETWORK_CATEGORY_ID="category_id";
    public static final String NETWORK_START = "start";
    public static final String NETWORK_LIMIT="limit";
    public static final String NETWORK_ORDER="order";
    public static final String NETWORK_ORDER_NAME = "name";
    public static final String NETWORK_SORT="sort";
    public static final String SORT_ORDER_ASC="asc";
    public static final String NETWORK_CUSTOMER_ID="customer_id";
    public static final String NETWORK_PRODUCT_ID="product_id";
    public static final String NETWORK_SHARE_TYPE="share_type";

    public static final String NETWORK_USER_TOKEN="user_token";
    public static final String NETWORK_IDENTITY_TOKEN="identity_token";
    public static final String NETWORK_IDENTITY_PROVIDER="identity_provider";
    public static final String NETWORK_USER_FIRST_NAME="user_first_name";
    public static final String NETWORK_USER_LAST_NAME="user_last_name";
    public static final String NETWORK_USER_GENDER="user_gender";
    public static final String NETWORK_USER_BIRTHDATE="user_birthdate";
    public static final String NETWORK_USER_EMAIL="user_email";

    public static final String TWITTER="twitter";
    public static final String FACEBOOK="facebook";
    public static final String PINTEREST="pinterest";

    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String MM_DD_YYYY ="MM-dd-yyyy";
    public static final String CATEGORY="category";
    public static final String CATEGORY_ID="category_id";
    public static final String CATEGORY_NAME="category_name";

    public static final String CACHE_DATA="cache_data";

    public static final String INTENT_FROM="intent_from";
    public static final String INTENT_FROM_CHECKOUT="intent_from_checkout";
    public static final String INTENT_FROM_PROFILE="intent_from_profile";
    public static final String INTENT_FROM_TABS="intent_from_tabs";
    public static final String INTENT_PRODUCT="intent_product";

    public static final String BROADCAST_DELETE_ITEM="broadcast_delete_item";
}
