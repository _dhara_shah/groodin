package com.grood.in.util;

import android.text.TextUtils;
import android.util.Log;

import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.grood.in.models.CartBean;
import com.grood.in.models.Product;
import com.grood.in.util.constant.Global;
import com.grood.in.util.search.CartPredicate;
import com.grood.in.util.search.ProductPredicate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USER on 16-08-2015.
 */
public class ItemCacheUtilities {
    private static List<CartBean> cartList;

    public static List<CartBean> getItemList() {
        cartList = (List<CartBean>)FileUtilities.readObjectFromFile(Global.CACHE_DATA);
        if(cartList == null) {
            cartList = new ArrayList<>();
        }
        return cartList;
    }

    public static void addToCart(Product product){
        List<CartBean> cartList = getItemList();
        CartBean cartObj = null;
        if(cartList.size() > 0) {
            cartObj = Iterables.find(cartList,
                    new ProductPredicate(product.getProductId()), null);
            if(cartObj != null) {
                // the product is present in the cart
                // update the quantity
                if (cartList.contains(cartObj)) {
                    cartList.remove(cartObj);
                }
            }
        }
        cartObj = addOrUpdateItem(cartObj, product);
        cartList.add(cartObj);
        FileUtilities.writeObjectToFile(cartList);
    }

    private static CartBean addOrUpdateItem(CartBean cartObj, Product product){
        if(cartObj != null){
            // the product is present in the cart
            // update the quantity
            increaseQty(cartObj,product);
        }else{
            double qty = (!TextUtils.isEmpty(product.getProductQuantity())) ?
                    Double.valueOf(product.getProductQuantity()) : 1.0;

            cartObj = new CartBean();
            cartObj.setQuantity(qty);

            double price = product.getProductPrice() * 1;
            cartObj.setTotalPrice(price);
            cartObj.setProductName(product.getProductName());
            cartObj.setPrice(product.getProductPrice());
            cartObj.setProductId(product.getProductId());
            cartObj.setProductImage(product.getProductImage());
        }
        return cartObj;
    }

    public static void deleteFromCart(CartBean cartObj){
        List<CartBean> cartList= getItemList();
        if(cartList  != null) {
            CartBean obj =  Iterables.find(cartList,new CartPredicate(cartObj), null);
            if(obj != null){
                cartList.remove(obj);
                FileUtilities.writeObjectToFile(cartList);
            }
        }
    }

    public static CartBean increaseQty(CartBean cartObj, Product product) {
        double productQty = (product == null ||
                (product != null && TextUtils.isEmpty(product.getProductQuantity()))) ?
                    1.0 : Double.parseDouble(product.getProductQuantity());

        double qty = cartObj.getQuantity();
        qty += productQty;

        double price = cartObj.getPrice();
        double totalPrice = price * qty;

        cartObj.setTotalPrice(totalPrice);
        cartObj.setQuantity(qty);
        return cartObj;
    }

    public static CartBean decreaseQty(CartBean cartObj) {
        double qty = cartObj.getQuantity();
        qty -= 1;

        double price = cartObj.getPrice();
        double totalPrice = price * qty;

        cartObj.setTotalPrice(totalPrice);
        cartObj.setQuantity(qty);
        return cartObj;
    }

    public static void updateListAndIncrease(CartBean cartObj) {
        List<CartBean> cartList = getItemList();

        CartBean obj =  Iterables.find(cartList, new CartPredicate(cartObj), null);
        if(obj != null){
            cartList.remove(obj);
        }

        cartObj = increaseQty(cartObj, null);
        cartList.add(cartObj);
        FileUtilities.writeObjectToFile(cartList);
    }

    public static void updateListAndDecrease(CartBean cartObj) {
        List<CartBean> cartList = getItemList();

        CartBean obj =  Iterables.find(cartList, new CartPredicate(cartObj), null);
        if(obj != null){
            cartList.remove(obj);
        }

        cartObj = decreaseQty(cartObj);
        if(cartObj.getQuantity() <= 0) {
            // do nothing
        }else {
            cartList.add(cartObj);
        }
        FileUtilities.writeObjectToFile(cartList);
    }

    public static double  getTotalPrice() {
        List<CartBean> cartList = getItemList();
        double totalPrice = 0.0;
        for(CartBean bean : cartList) {
            double price = bean.getQuantity() *  bean.getPrice();
            Log.e("dhara", "price >>>> " + bean.getPrice());
            Log.e("dhara", "qty >>>> " + bean.getQuantity());
            Log.e("dhara", "total >>>> " + bean.getTotalPrice());
            Log.e("dhara","calculated total >>>> " + price);
            totalPrice += price;
        }
        return totalPrice;
    }
}
