package com.grood.in.util;

import com.grood.in.util.constant.Global;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by USER on 08-08-2015.
 */
public class ValidationUtils {
    private static final String NAME = "^[\\p{L} .'-]+$";

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public final static boolean isNameValid(String value) {
        Pattern pattern = Pattern.compile(NAME);
        Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }

    public final static boolean isPasswordValid(String value){
        if(value.length() < 5 ||
                value.length() > 16) {
            return false;
        }else {
            return true;
        }
    }

    public final static boolean isDateValid(String dtValue)  {
        SimpleDateFormat sdf = new SimpleDateFormat(Global.MM_DD_YYYY,
                Locale.ENGLISH);
        try {
            Date dt = sdf.parse(dtValue);
            Date dtNow = sdf.parse(sdf.format(new Date()));

            if(dtNow.before(dt)) {
                return false;
            }else {
                if(dtNow.equals(dt)) {
                    return false;
                }
                return true;
            }
        }catch (ParseException e){
            e.printStackTrace();
        }
        return false;
    }
}
