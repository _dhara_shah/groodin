package com.grood.in.util.search;

import com.google.common.base.Predicate;
import com.grood.in.models.Category;

import java.util.Locale;

/**
 * Created by USER on 30-08-2015.
 */
public class SubcategoryPredicate implements Predicate<Category> {
    private String mSearchString;
    public SubcategoryPredicate(String searchString) {
        mSearchString = searchString;
    }

    @Override
    public boolean apply(Category category) {
        if(category.getName().toLowerCase(Locale.ENGLISH)
                .contains(mSearchString.toLowerCase(Locale.ENGLISH))){
            return true;
        }
        return false;
    }
}
