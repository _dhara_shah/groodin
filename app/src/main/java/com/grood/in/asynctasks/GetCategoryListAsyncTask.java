package com.grood.in.asynctasks;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.models.CategoryListHolder;
import com.grood.in.util.network.NetworkUtilities;

/**
 * Created by USER on 09-08-2015.
 */
public class GetCategoryListAsyncTask extends AsyncTask<Void,Void,CategoryListHolder> {
    private Dialog mDialog;
    private Context mContext;
    private ResponseListener mListener;

    public GetCategoryListAsyncTask(Context context, ResponseListener listener) {
        mContext = context;
        mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = CustomDialog.showProgressDialog(mContext);
        mDialog.show();
    }

    @Override
    protected CategoryListHolder doInBackground(Void... params) {
        String response = NetworkUtilities.makeWebserviceCall(NetworkUtilities.METHOD.GET,
                NetworkUtilities.NETWORK_FUNCTIONS.GetCategories,
                null);
        if(response != null) {
            try{
                Gson gson = new Gson();
                CategoryListHolder holder = gson.fromJson(response,
                        CategoryListHolder.class);
                return holder;
            }catch (JsonSyntaxException e){
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(CategoryListHolder categoryListHolder) {
        super.onPostExecute(categoryListHolder);
        if(mDialog!= null) {mDialog.dismiss();}
        if(mListener != null)  {mListener.onResponseReceived(categoryListHolder);}
    }
}
