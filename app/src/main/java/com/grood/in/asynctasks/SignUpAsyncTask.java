package com.grood.in.asynctasks;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.models.SignUpResponse;
import com.grood.in.models.User;
import com.grood.in.prefs.SharedPrefs;
import com.grood.in.util.constant.Global;
import com.grood.in.util.network.NetworkUtilities;

import java.util.HashMap;

/**
 * Created by USER on 08-08-2015.
 */
public class SignUpAsyncTask extends AsyncTask<Void,Void,SignUpResponse> {
    private ResponseListener mListener;
    private Context mContext;
    private User mUser;
    private Dialog mDialog;

    public SignUpAsyncTask(Context context,User user, ResponseListener listener){
        mContext = context;
        mListener = listener;
        mUser = user;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = CustomDialog.showProgressDialog(mContext);
        mDialog.show();
    }

    @Override
    protected SignUpResponse doInBackground(Void... params) {
        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put(Global.NETWORK_EMAIL,mUser.getEmail());
        hashMap.put(Global.NETWORK_FIRSTNAME, mUser.getFirstName());
        hashMap.put(Global.NETWORK_LASTNAME, mUser.getLastName());
        hashMap.put(Global.NETWORK_BIRTHDAY, mUser.getBirthday());
        hashMap.put(Global.NETWORK_ID_GENDER,mUser.getGenderId());
        hashMap.put(Global.NETWORK_PASSWD,mUser.getPassword());

        String response = NetworkUtilities.makeWebserviceCall(NetworkUtilities.METHOD.POST,
                NetworkUtilities.NETWORK_FUNCTIONS.SignUp,
                hashMap);

        if(response != null) {
            try {
                Gson gson  = new Gson();
                SignUpResponse holder = gson.fromJson(response, SignUpResponse.class);
                return holder;
            }catch (JsonSyntaxException e){
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(SignUpResponse responseHolder) {
        super.onPostExecute(responseHolder);
        if(mDialog != null) { mDialog.dismiss(); }

        if(responseHolder != null) {
            if(responseHolder.getResponse().getStatus().equals("1")) {
                SharedPrefs.putString(SharedPrefs.USER_ID,
                        responseHolder.getResponse().getCustomerHolder().getCustomer().getId());
            }
        }

        if(mListener != null) {
            mListener.onResponseReceived(responseHolder);
        }
    }
}
