package com.grood.in.asynctasks;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.models.ProfileResponseHolder;
import com.grood.in.prefs.SharedPrefs;
import com.grood.in.util.constant.Global;
import com.grood.in.util.network.NetworkUtilities;

import org.androidannotations.annotations.sharedpreferences.SharedPref;

import java.util.HashMap;

/**
 * Created by USER on 23-08-2015.
 */
public class GetUserDataAsyncTask extends AsyncTask<Void,Void,ProfileResponseHolder> {
    private Context mContext;
    private ResponseListener mListener;
    private Dialog mDialog;

    public GetUserDataAsyncTask(Context context, ResponseListener listener){
        mContext = context;
        mListener =  listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = CustomDialog.showProgressDialog(mContext);
        mDialog.show();
    }

    @Override
    protected ProfileResponseHolder doInBackground(Void... params) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Global.NETWORK_CUSTOMER_ID,
                SharedPrefs.getString(SharedPrefs.USER_ID,""));
        String response = NetworkUtilities.makeWebserviceCall(NetworkUtilities.METHOD.POST,
                NetworkUtilities.NETWORK_FUNCTIONS.GetProfile, hashMap);
        if(response!= null){
            try {
                Gson gson = new Gson();
                ProfileResponseHolder  holder =
                        gson.fromJson(response, ProfileResponseHolder.class);
                return holder;
            }catch (JsonSyntaxException  e){
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(ProfileResponseHolder profileResponseHolder) {
        super.onPostExecute(profileResponseHolder);
        if(mDialog!= null){mDialog.dismiss();}
        if(mListener != null){mListener.onResponseReceived(profileResponseHolder);}
    }
}


/*
[22-08-2015 21:37:02] Dhaval Kanojiya: https://grood.in/module/skeleton/index.php?fc=module&module=skeleton&controller=account&action=getprofile
[22-08-2015 21:37:10] Dhaval Kanojiya: customer_id
[22-08-2015 21:37:27] Dhaval Kanojiya: {
  "response": {
    "status": "1",
    "data": {
      "customer_id": 51,
      "fristname": "ggggggg",
      "lastname": "kanojiya",
      "id_gender": "1",
      "birthday": "1999-10-18",
      "email": "dhavalkanojiya4@gmail.com",
      "addessses": [
        {
          "id_address": "27",
          "firstname": "ggggggg",
          "lastname": "kanojiya",
          "address1": "fffdfdf",
          "address2": "fdfdfdfdf",
          "postcode": "380026",
          "city": "ahemdabad",
          "phone_mobile": "7383323653"
        },
        {
          "id_address": "28",
          "firstname": "ggggggg",
          "lastname": "kanojiya",
          "address1": "fffdfdf",
          "address2": "fdfdfdfdf",
          "postcode": "380026",
          "city": "ahemdabad",
          "phone_mobile": "7383323653"
        }
      ],
      "order_history": [
        {
          "id_order": "35",
          "reference": "EJQMRPQSC",
          "payment": "Cash on delivery (COD)",
          "total_paid": "32.000000",
          "order_state": "Preparation in progress",
          "date_add": "2015-08-22 21:14:45"
        },
        {
          "id_order": "34",
          "reference": "QAVACOGSQ",
          "payment": "Cash on delivery (COD)",
          "total_paid": "40.000000",
          "order_state": "Preparation in progress",
          "date_add": "2015-08-22 20:17:07"
        }
      ]
    }
  }
}
 */