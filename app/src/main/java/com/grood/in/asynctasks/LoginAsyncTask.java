package com.grood.in.asynctasks;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.models.LoginResponseHolder;
import com.grood.in.prefs.SharedPrefs;
import com.grood.in.util.constant.Global;
import com.grood.in.util.network.NetworkUtilities;

import java.util.HashMap;

/**
 * Created by USER on 08-08-2015.
 */
public class LoginAsyncTask extends AsyncTask<Void,Void,LoginResponseHolder>{
    private ResponseListener mListener;
    private Context mContext;
    private String mEmail;
    private String mPassword;
    private Dialog mDialog;

    public LoginAsyncTask(Context context,String email, String password, ResponseListener listener){
        mContext = context;
        mListener = listener;
        mEmail = email;
        mPassword = password;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = CustomDialog.showProgressDialog(mContext);
        mDialog.show();
    }

    @Override
    protected LoginResponseHolder doInBackground(Void... params) {
        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put(Global.NETWORK_EMAIL, mEmail);
        hashMap.put(Global.NETWORK_PASSWORD, mPassword);

        String response = NetworkUtilities.makeWebserviceCall(NetworkUtilities.METHOD.POST,
                NetworkUtilities.NETWORK_FUNCTIONS.Login,
                hashMap);
        if(response != null) {
            try{
                Gson gson = new Gson();
                LoginResponseHolder holder =
                        gson.fromJson(response, LoginResponseHolder.class);
                return holder;
            }catch (JsonSyntaxException e){
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(LoginResponseHolder loginResponse) {
        super.onPostExecute(loginResponse);
        if(mDialog != null) { mDialog.dismiss(); }
        if(loginResponse != null) {
            if(loginResponse.getLoginResponse().getStatus().equals("1")) {
                SharedPrefs.putString(SharedPrefs.USER_ID,
                        loginResponse.getLoginResponse().getCustomer().getId());
            }
        }
        if(mListener != null) {
            mListener.onResponseReceived(loginResponse);
        }
    }
}
