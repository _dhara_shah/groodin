package com.grood.in.asynctasks;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;

import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.util.FileUtilities;

import java.io.File;

/**
 * Created by USER on 23-08-2015.
 */
public class DownloadFileAsyncTask extends AsyncTask<Void,Void,String>{
    private Context mContext;
    private String mImageUrl;
    private Dialog mDialog;
    private ResponseListener mListener;

    public DownloadFileAsyncTask(Context context, String imageUrl, ResponseListener listener){
        mContext = context;
        mImageUrl = imageUrl;
        mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = CustomDialog.showProgressDialog(mContext);
        mDialog.show();
    }

    @Override
    protected String doInBackground(Void... params) {
        File file = FileUtilities.downloadImage(mImageUrl);
        return file.getAbsolutePath();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(mDialog != null){mDialog.dismiss();}
        if(mListener != null){mListener.onResponseReceived(s);}
    }
}
