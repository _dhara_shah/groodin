package com.grood.in.asynctasks;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;

import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.models.ShareResponseHolder;

/**
 * Created by USER on 23-08-2015.
 */
public class ShareAsyncTask extends AsyncTask<Void,Void,ShareResponseHolder>{
    private Context mContext;
    private Dialog mDialog;
    private String mProductId;
    private String mShareType;
    private ResponseListener mListener;

    public ShareAsyncTask(Context context,String productId,
                          String shareType, ResponseListener listener){
        mContext =context;
        mListener = listener;
        mProductId = productId;
        mShareType = shareType;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = CustomDialog.showProgressDialog(mContext);
        mDialog.show();
    }

    @Override
    protected ShareResponseHolder doInBackground(Void... params) {

        return null;
    }

    @Override
    protected void onPostExecute(ShareResponseHolder shareResponseHolder) {
        super.onPostExecute(shareResponseHolder);
        if(mDialog != null){mDialog.dismiss();}
    }
}

/*
[22-08-2015 22:02:08] Dhaval Kanojiya: https://grood.in/module/skeleton/index.php?fc=module&module=skeleton&controller=category&action=productshare
[22-08-2015 22:02:14] Dhaval Kanojiya: product_id
[22-08-2015 22:02:17] Dhaval Kanojiya: share_type
[22-08-2015 22:02:37] Dhaval Kanojiya: facebook or twitter or pintrest
[22-08-2015 22:02:46] Dhaval Kanojiya: {
  "response": {
    "status": "1",
    "data": {
      "fb_count": "80",
      "twitter_count": "125",
      "pintrest_count": "225"
    }
  }
}
 */