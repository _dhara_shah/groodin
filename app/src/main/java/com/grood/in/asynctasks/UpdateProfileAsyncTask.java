package com.grood.in.asynctasks;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.models.User;
import com.grood.in.prefs.SharedPrefs;
import com.grood.in.util.constant.Global;
import com.grood.in.util.network.NetworkUtilities;

import java.util.HashMap;

/**
 * Created by USER on 23-08-2015.
 */
public class UpdateProfileAsyncTask extends AsyncTask<Void,Void,Void> {
    private Context mContext;
    private ResponseListener mListener;
    private User mUser;
    private Dialog mDialog;

    public UpdateProfileAsyncTask(Context context,User user, ResponseListener listener){
        mContext = context;
        mUser  = user;
        mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = CustomDialog.showProgressDialog(mContext);
        mDialog.show();
    }

    @Override
    protected Void doInBackground(Void... params) {
        HashMap<String, String > hashMap  = new HashMap<>();
        hashMap.put(Global.NETWORK_CUSTOMER_ID, SharedPrefs.getString(SharedPrefs.USER_ID, ""));
        hashMap.put(Global.NETWORK_USER_EMAIL, mUser.getEmail());
        hashMap.put(Global.NETWORK_BIRTHDAY, mUser.getBirthday());
        hashMap.put(Global.NETWORK_LASTNAME,  mUser.getLastName());
        hashMap.put(Global.NETWORK_FIRSTNAME, mUser.getFirstName());
        hashMap.put(Global.NETWORK_GENDER, mUser.getGenderId());
        hashMap.put(Global.NETWORK_PASSWD, mUser.getNewPassword());

        String response = NetworkUtilities.makeWebserviceCall(NetworkUtilities.METHOD.POST,
                NetworkUtilities.NETWORK_FUNCTIONS.UpdateProfile,
                hashMap);

        if(response != null) {
            try {
                Gson gson =  new Gson();
            }catch (JsonSyntaxException e){
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(mDialog != null){mDialog.dismiss();}
        if(mListener !=  null)  {mListener.onResponseReceived(aVoid);}
    }
}
