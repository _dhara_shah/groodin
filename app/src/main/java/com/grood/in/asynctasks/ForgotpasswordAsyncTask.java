package com.grood.in.asynctasks;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.models.ForgotPasswordHolder;
import com.grood.in.util.constant.Global;
import com.grood.in.util.network.NetworkUtilities;

import java.util.HashMap;

/**
 * Created by USER on 09-08-2015.
 */
public class ForgotpasswordAsyncTask extends AsyncTask<Void,Void,ForgotPasswordHolder>{
    private Context mContext;
    private String mEmail;
    private ResponseListener mListener;
    private Dialog mDialog;

    public ForgotpasswordAsyncTask(Context context, String email,
                                   ResponseListener listener) {
        mContext = context;
        mEmail = email;
        mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = CustomDialog.showProgressDialog(mContext);
        mDialog.show();
    }

    @Override
    protected ForgotPasswordHolder doInBackground(Void... params) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Global.NETWORK_EMAIL, mEmail);

        String response = NetworkUtilities.makeWebserviceCall(NetworkUtilities.METHOD.POST,
                NetworkUtilities.NETWORK_FUNCTIONS.ForgotPassword,
                hashMap);

        if(response != null) {
            try {
                Gson gson = new Gson();
                ForgotPasswordHolder holder =
                        gson.fromJson(response, ForgotPasswordHolder.class);
                return holder;
            }catch (JsonSyntaxException e) {
                e.printStackTrace();
            }

        }

        return null;
    }

    @Override
    protected void onPostExecute(ForgotPasswordHolder forgotPasswordHolder) {
        super.onPostExecute(forgotPasswordHolder);
        if(mDialog!=null) {mDialog.dismiss();}

        if(mListener != null) {
            mListener.onResponseReceived(forgotPasswordHolder);
        }
    }
}
