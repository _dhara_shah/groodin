package com.grood.in.asynctasks;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.models.SocialUser;
import com.grood.in.models.SocialUserResponseHolder;
import com.grood.in.prefs.SharedPrefs;
import com.grood.in.util.constant.Global;
import com.grood.in.util.network.NetworkUtilities;

import java.util.HashMap;

/**
 * Created by USER on 19-08-2015.
 */
public class SocialLoginAsynctask extends AsyncTask<Void,Void,SocialUserResponseHolder>{
    private SocialUser mSocialUser;
    private ResponseListener mListener;
    private Context mContext;
    private Dialog mDialog;

    public SocialLoginAsynctask(Context context, SocialUser socialUser, ResponseListener listener) {
        mSocialUser = socialUser;
        mContext = context;
        mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = CustomDialog.showProgressDialog(mContext);
        mDialog.show();
    }

    @Override
    protected SocialUserResponseHolder doInBackground(Void... params) {
        HashMap<String,String> hashMap = new HashMap<>();

        hashMap.put(Global.NETWORK_USER_FIRST_NAME, mSocialUser.getFirstName());
        hashMap.put(Global.NETWORK_USER_LAST_NAME, (mSocialUser.getLastName() != null)  ? mSocialUser.getLastName() : "");
        hashMap.put(Global.NETWORK_USER_BIRTHDATE, (mSocialUser.getBirthdate() != null) ?  mSocialUser.getBirthdate() :  "");
        hashMap.put(Global.NETWORK_USER_EMAIL, (mSocialUser.getEmail() != null) ? mSocialUser.getEmail() : "");
        hashMap.put(Global.NETWORK_USER_GENDER, (mSocialUser.getGender() !=  null) ? mSocialUser.getGender() : "");
        hashMap.put(Global.NETWORK_IDENTITY_TOKEN, mSocialUser.getUserId());
        hashMap.put(Global.NETWORK_IDENTITY_PROVIDER, mSocialUser.getSocialLoginType());
        hashMap.put(Global.NETWORK_USER_TOKEN, mSocialUser.getUserToken());

        String response = NetworkUtilities.makeWebserviceCall(NetworkUtilities.METHOD.POST,
                NetworkUtilities.NETWORK_FUNCTIONS.SocialLogin,
                hashMap);

        if(response != null) {
            try {
                Gson gson = new Gson();
                SocialUserResponseHolder holder=
                        gson.fromJson(response,SocialUserResponseHolder.class);
                return holder;
            }catch (JsonSyntaxException e){
                e.printStackTrace();
            }
        }
         return null;
    }

    @Override
    protected void onPostExecute(SocialUserResponseHolder aVoid) {
        super.onPostExecute(aVoid);
        if (mDialog != null) {
            mDialog.dismiss();
        }

        if(aVoid!=null) {
            if(aVoid.getResponse().getStatus().equals("1")) {
                SharedPrefs.putString(SharedPrefs.USER_ID,
                        aVoid.getResponse().getSocialUserBean().getUserId());
            }
        }

        if(mListener != null)  {
            mListener.onResponseReceived(aVoid);
        }
    }

    /*
    [21-08-2015 19:27:04] Dhaval kanojiya: sucess login

response :
status= 1
data = {
confirmation = 1
customer_id =
}

sucess signup & login

response :
status= 1
data = {
confirmation = 2
customer_id =
}
[21-08-2015 19:28:26] Dhaval kanojiya: device token error

response :
status= -1
msg = 'Device Token Required
     */
}
