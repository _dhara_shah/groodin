package com.grood.in.asynctasks;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.models.ProductListHolder;
import com.grood.in.models.TotalProductCountHolder;
import com.grood.in.util.constant.Global;
import com.grood.in.util.network.NetworkUtilities;

import java.util.HashMap;

/**
 * Created by USER on 10-08-2015.
 */
public class GetProductListAsyncTask extends AsyncTask<Void,Void, ProductListHolder> {
    private Context mContext;
    private String mCatId;
    private Dialog mDialog;
    private String mStart;
    private String mLimit;
    private ResponseListener mListener;

    public GetProductListAsyncTask(Context context,String catId,
                                   String start, String limit, ResponseListener listener) {
        mCatId = catId;
        mContext = context;
        mListener = listener;
        mStart = start;
        mLimit = limit;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = CustomDialog.showProgressDialog(mContext);
        mDialog.show();
    }

    @Override
    protected ProductListHolder doInBackground(Void... params) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Global.NETWORK_CATEGORY_ID, mCatId);
        hashMap.put(Global.NETWORK_ORDER, Global.NETWORK_ORDER_NAME);
        hashMap.put(Global.NETWORK_START,mStart);
        hashMap.put(Global.NETWORK_LIMIT, mLimit);
        hashMap.put(Global.NETWORK_SORT, Global.SORT_ORDER_ASC);

        String response = NetworkUtilities.makeWebserviceCall(NetworkUtilities.METHOD.POST,
                NetworkUtilities.NETWORK_FUNCTIONS.GetProductList,
                hashMap);
        if(response != null) {
            try {
                Gson gson = new Gson();
                ProductListHolder holder =
                        gson.fromJson(response, ProductListHolder.class);
                return holder;
            }catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(ProductListHolder productListHolder) {
        super.onPostExecute(productListHolder);
        if(mDialog  != null) {mDialog.dismiss();}
        if(mListener  != null) {mListener.onResponseReceived(productListHolder);}
    }
}
