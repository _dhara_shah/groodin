package com.grood.in.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RadioButton;

import com.grood.in.GroodApp;
import com.grood.in.R;
import com.grood.in.activities.HomeActivity;
import com.grood.in.activities.LoginActivity;
import com.grood.in.activities.LoginActivity_;
import com.grood.in.adapters.CheckoutAdapter;
import com.grood.in.asynctasks.SendOrderAsyncTask;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.customviews.CustomTextView;
import com.grood.in.interfaces.ICartListener;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.models.CartBean;
import com.grood.in.prefs.SharedPrefs;
import com.grood.in.util.CommonUtilities;
import com.grood.in.util.ItemCacheUtilities;
import com.grood.in.util.constant.Global;
import com.grood.in.util.network.NetworkUtilities;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.ViewById;

import java.util.List;

/**
 * Created by USER on 21-08-2015.
 */
@EFragment(R.layout.fragment_checkout)
public class ConfirmOderFragment extends ToolbarFragment implements ICartListener, ResponseListener {
    private static ConfirmOderFragment_ mFragment;
    private ICartListener mCartListener;

    public static ConfirmOderFragment_ newInstance() {
        mFragment  = new ConfirmOderFragment_();
        return mFragment;
    }

    public static ConfirmOderFragment_ newInstance(String from){
        mFragment  = new ConfirmOderFragment_();
        Bundle args = new Bundle();
        args.putString(Global.INTENT_FROM, from);
        mFragment.setArguments(args);
        return mFragment;
    }

    CustomTextView txtTotal;
    ListView mLstView;

    @ViewById(R.id.rdbtnCash)
    RadioButton rdbtnCash;

    @ViewById(R.id.rdbtnMobo)
    RadioButton rdbtnMobo;


    @App
    GroodApp mApp;

    private CheckoutAdapter mCheckoutAdapter;
    private List<CartBean> cartBeanList;
    private ResponseListener mListener;

    @Receiver(actions = Global.BROADCAST_DELETE_ITEM, registerAt = Receiver.RegisterAt.OnStartOnStop)
    protected void deleted() {
        // Will be called when an org.androidannotations.ACTION_1 intent is sent.
        double totalPrice = ItemCacheUtilities.getTotalPrice();
        txtTotal.setText(" र "+totalPrice);
        CommonUtilities.setListViewHeightBasedOnChildren(mLstView);
    }

    @Click(R.id.relLeft)
    public void backClicked(){
        getActivity().onBackPressed();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view   = getActivity().getLayoutInflater().inflate(R.layout.fragment_checkout, null);
        setupActionbarFragment(view);
        mRelCart.setVisibility(View.GONE);
        mCartListener = this;
        mListener = this;

        if(getArguments() != null  && getArguments().containsKey(Global.INTENT_FROM)) {
            mToolbar.setVisibility(View.GONE);
        }

        mToolbar.setBackgroundColor(mApp.getResources().getColor(R.color.colorPrimaryOrange));
        mImgBack.setImageResource(R.mipmap.ic_back);

        mTxtHeader.setText(mApp.getResources().getString(R.string.checkout_header));
        mImgLogo.setVisibility(View.GONE);
        mTxtHeader.setVisibility(View.VISIBLE);

        txtTotal = (CustomTextView)view.findViewById(R.id.txtTotal);
        mLstView = (ListView)view.findViewById(android.R.id.list);

        double totalPrice = ItemCacheUtilities.getTotalPrice();
        txtTotal.setText(" र " + totalPrice);

        cartBeanList = ItemCacheUtilities.getItemList();
        mCheckoutAdapter = new CheckoutAdapter(getActivity(),
                R.layout.individual_checkout,
                cartBeanList,
                mCartListener);
        mLstView.setAdapter(mCheckoutAdapter);
        CommonUtilities.setListViewHeightBasedOnChildren(mLstView);
        return view;
    }


    @Click(R.id.btnPlaceOrder)
    public void placeOrder(){
        if(SharedPrefs.getString(SharedPrefs.USER_ID,"").trim().length() <= 0) {
            Intent intent = new Intent(mApp,
                    LoginActivity_.class);
            intent.putExtra(Global.INTENT_FROM, Global.INTENT_FROM_CHECKOUT);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }else {
            // make webservice call
            if (NetworkUtilities.isNetworkAvailable()) {
                new SendOrderAsyncTask(getActivity(), mListener).execute();
            } else {
                // no internet
                CustomDialog.showAlertDialog(getActivity(),
                        mApp.getResources().getString(R.string.no_internet));
                return;
            }
        }
    }

    @Click(R.id.btnCancel)
    public void cancel() {
        Intent intent = new Intent(mApp,
                HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Click(R.id.relCash)
    public void cashSelection(){
        rdbtnCash.setChecked(true);
        rdbtnMobo.setChecked(false);
    }

    @Click(R.id.relMobo)
    public void moboSelection(){
        rdbtnMobo.setChecked(true);
        rdbtnCash.setChecked(false);
    }

    @Override
    public void onResponseReceived(Object object) {

    }
}
