package com.grood.in.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.grood.in.R;
import com.grood.in.customviews.CustomTextView;
import com.grood.in.interfaces.ICartListener;
import com.grood.in.models.CartBean;
import com.grood.in.util.ItemCacheUtilities;

/**
 * Created by USER on 10-08-2015.
 */
public class ToolbarFragment extends Fragment implements ICartListener{
    public Toolbar mToolbar;
    protected CustomTextView mTxtItemCount;
    protected CustomTextView mTxtHeader;
    protected RelativeLayout mRelCart;
    protected RelativeLayout mRelLeft;
    protected ImageView mImgBack;
    protected ImageView mImgLogo;
    public ICartListener mCartListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCartListener = this;
    }

    public void setupActionbarFragment(View view) {
        mToolbar = (Toolbar)view.findViewById(R.id.toolbar);
        mTxtItemCount = (CustomTextView)view.findViewById(R.id.txtItemCount);
        mTxtHeader = (CustomTextView)view.findViewById(R.id.txtHeader);
        mImgBack = (ImageView)view.findViewById(R.id.imgBack);
        mRelCart = (RelativeLayout)view.findViewById(R.id.relCart);
        mRelLeft = (RelativeLayout)view.findViewById(R.id.relLeft);
        mImgLogo = (ImageView)view.findViewById(R.id.imgLogo);

        mImgBack.setBackgroundResource(R.mipmap.ic_back);

        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(null);
        mToolbar.setLogo(null);

        updateItemCount();
    }

    protected void updateItemCount(){
        mTxtItemCount.setText("" + ItemCacheUtilities.getItemList().size());
    }

    @Override
    public void onItemQtyIncreased(CartBean cartBean) {
        ItemCacheUtilities.updateListAndIncrease(cartBean);
        updateItemCount();
    }

    @Override
    public void onItemQtyDecreased(CartBean cartBean) {
        ItemCacheUtilities.updateListAndDecrease(cartBean);
        updateItemCount();
    }

    @Override
    public void onItemDeleted(CartBean cartBean) {
        ItemCacheUtilities.deleteFromCart(cartBean);
        if(mRelCart.getVisibility() == View.VISIBLE) {
            updateItemCount();
        }
     }
}
