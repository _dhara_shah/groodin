package com.grood.in.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethod;
import android.view.inputmethod.InputMethodManager;
import android.widget.ExpandableListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.grood.in.GroodApp;
import com.grood.in.R;
import com.grood.in.activities.HomeActivity;
import com.grood.in.activities.ProductActivity_;
import com.grood.in.activities.SubCategoryActivity;
import com.grood.in.activities.SubCategoryActivity_;
import com.grood.in.adapters.CategoryAdapter;
import com.grood.in.asynctasks.GetCategoryListAsyncTask;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.customviews.CustomEditText;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.models.Category;
import com.grood.in.models.CategoryListHolder;
import com.grood.in.util.CommonUtilities;
import com.grood.in.util.constant.Global;
import com.grood.in.util.network.NetworkUtilities;
import com.grood.in.util.search.CategoryPredicate;
import com.grood.in.util.search.SubcategoryPredicate;
import com.grood.in.util.staticmanager.StaticManager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.CancellationException;

/**
 * Created by USER on 09-08-2015.
 */
@EFragment(R.layout.fragment_category)
public class CategoryFragment extends BaseFragment implements ResponseListener, ExpandableListView.OnGroupExpandListener, ExpandableListView.OnChildClickListener {
    private static CategoryFragment_ mFragment;
    private ResponseListener mListener;
    private CategoryAdapter mAdapter;
    private List<Category> mCategoryList;
    private List<Category> mCategoryListOriginal;

    @App
    GroodApp mApp;

    ExpandableListView mListView;
    CustomEditText mEditSearch;

    public static CategoryFragment_ newInstance() {
        mFragment = new CategoryFragment_();
        return mFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        View view = inflater.inflate(R.layout.fragment_category,container,false);
        mListView = (ExpandableListView)view.findViewById(R.id.categoryList);
        mEditSearch = (CustomEditText)view.findViewById(R.id.editSearch);

        mListener = this;
        mListView.setOnChildClickListener(this);
        mListView.setOnGroupExpandListener(this);
        mCategoryList = new ArrayList<>();

        if(NetworkUtilities.isNetworkAvailable()) {
            new GetCategoryListAsyncTask(getActivity(), mListener).execute();
        }else {
            CategoryListHolder holder = StaticManager.getCategories();
            onResponseReceived(holder);
        }

        setListener();
        return view;
    }

    private void setListener(){
        mEditSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (!TextUtils.isEmpty(mEditSearch.getText().toString())) {
                        Iterable<Category> categoryList = Iterables.filter(mCategoryListOriginal,
                                new CategoryPredicate(mEditSearch.getText().toString()));
                        List<Category> newList = Arrays.asList(Iterables.toArray(categoryList, Category.class));
                        if (newList != null && newList.size() > 0) {
                            int counter = 0;
                            for (Category category : newList) {
                                if (category.getSubcategory() != null) {
                                    Collection<Category> subCollection =
                                            Collections2.filter(category.getSubcategory(),
                                                    new SubcategoryPredicate(mEditSearch.getText().toString()));
                                    if (subCollection == null) {
                                        category.setSubcategory(null);
                                    } else {
                                        List<Category> subList = new ArrayList<>();
                                        subList.addAll(subCollection);
                                        category.setSubcategory(subList);
                                    }
                                }
                            }
                        }
                        setAdapter(newList);
                    } else {
                        setAdapter((List<Category>)CommonUtilities.deepClone(mCategoryListOriginal));
                    }
                    InputMethodManager inputManager = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        mEditSearch.addTextChangedListener(new MyTextWatcher());
    }

    @Override
    public void onResponseReceived(Object object) {
        if(object != null) {
            if(object instanceof CategoryListHolder) {
                CategoryListHolder holder = (CategoryListHolder)object;
                if(holder.getResponse().getStatus().equals("1")) {
                    mCategoryListOriginal=
                            (List<Category>) CommonUtilities.deepClone(holder.getResponse().getCategoryList());
                    setAdapter(holder.getResponse().getCategoryList());
                }else {
                    setAdapter(null);
                }
            }
        }
    }

    private void setAdapter(List<Category> categoryList){
        if(categoryList == null) {
            categoryList = new ArrayList<>();
        }
        mCategoryList.clear();
        mCategoryList.addAll(categoryList);

        mAdapter = new CategoryAdapter(getActivity(),
                mCategoryList);
        mListView.setAdapter(mAdapter);
    }

    @Override
    public void onGroupExpand(int groupPosition) {
        for(int i = 0 ;i<mCategoryList.size();i++){
            if(i==groupPosition) {
                //mListView.expandGroup(groupPosition);
            }else {
                mListView.collapseGroup(i);
            }
        }
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v,
                                int groupPosition, int childPosition, long id) {
        if(mCategoryList.get(groupPosition).getSubcategory().get(childPosition).getSubSubCategory()!=null &&
                mCategoryList.get(groupPosition).getSubcategory().get(childPosition).getSubSubCategory().size() > 0){
            Intent intent = new Intent(mApp,
                    SubCategoryActivity_.class);
            intent.putExtra(Global.CATEGORY,
                    mCategoryList.get(groupPosition).getSubcategory().get(childPosition));
            intent.putExtra(Global.CATEGORY_ID,mCategoryList.get(groupPosition).getId());
            intent.putExtra(Global.CATEGORY_NAME, mCategoryList.get(groupPosition).getName());
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }else {
            Intent intent = new Intent(mApp,
                    ProductActivity_.class);
            intent.putExtra(Global.CATEGORY_ID, mCategoryList.get(groupPosition).getId());
            intent.putExtra(Global.CATEGORY,mCategoryList.get(groupPosition).getSubcategory().get(childPosition) );
            intent.putExtra(Global.CATEGORY_NAME,mCategoryList.get(groupPosition).getName() );
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
        return true;
    }

    class MyTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(TextUtils.isEmpty(s.toString())) {
                setAdapter((List<Category>)CommonUtilities.deepClone(mCategoryListOriginal));
            }
        }
    }
}
