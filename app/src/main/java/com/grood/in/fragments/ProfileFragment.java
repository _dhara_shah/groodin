package com.grood.in.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.grood.in.GroodApp;
import com.grood.in.R;
import com.grood.in.adapters.AddressAdapter;
import com.grood.in.adapters.GenderAdapter;
import com.grood.in.adapters.OrderHistoryAdapter;
import com.grood.in.asynctasks.GetUserDataAsyncTask;
import com.grood.in.asynctasks.UpdateProfileAsyncTask;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.customdialogs.DatePickerDialogFragment;
import com.grood.in.customviews.CustomButton;
import com.grood.in.customviews.CustomEditText;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.models.Address;
import com.grood.in.models.OrderHistory;
import com.grood.in.models.ProfileResponseHolder;
import com.grood.in.models.User;
import com.grood.in.models.UserProfile;
import com.grood.in.util.ValidationUtils;
import com.grood.in.util.constant.Global;
import com.grood.in.util.network.NetworkUtilities;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemSelect;
import org.androidannotations.annotations.Touch;
import org.androidannotations.annotations.ViewById;
import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by USER on 22-08-2015.
 */
@EFragment(R.layout.fragment_profile)
public class ProfileFragment  extends ToolbarFragment implements DatePickerDialog.OnDateSetListener, ResponseListener, AdapterView.OnItemSelectedListener {
    private static ProfileFragment_ mFragment;
    private ResponseListener mListener;

    public static ProfileFragment_  newInstance() {
        mFragment = new ProfileFragment_();
        return  mFragment;
    }

    SimpleDateFormat mSdf;

    @ViewById(R.id.card_view_list_address)
    CardView addressCardView;

    @ViewById(R.id.card_view_profile_fields)
    CardView profileCard;

    @ViewById(R.id.card_view_order)
    CardView orderHistoryCard;

    @App
    GroodApp mApp;

    @Click(R.id.txtOrderHistory)
    public void orderHistory()  {
        if(orderHistoryCard.getVisibility()== View.VISIBLE){
            orderHistoryCard.setVisibility(View.GONE);
        }else {
            orderHistoryCard.setVisibility(View.VISIBLE);
        }
    }

    @Click(R.id.txtMyAddresses)
    public void myAddresses() {
        if(addressCardView.getVisibility()== View.VISIBLE){
            addressCardView.setVisibility(View.GONE);
        }else {
            addressCardView.setVisibility(View.VISIBLE);
        }
    }

    @Click(R.id.txtMyPersonalInfo)
    public void personalInfo() {
        if(profileCard.getVisibility()== View.VISIBLE){
            profileCard.setVisibility(View.GONE);
        }else {
            profileCard.setVisibility(View.VISIBLE);
        }
    }

    @ViewById(R.id.list_addresses)
    ListView listViewAddress;

    @ViewById(R.id.list_order_history)
    ListView listViewOrderHistory;

    @ViewById(R.id.btnUpdate)
    CustomButton btnUpdate;

    @ViewById(R.id.btnAddNew)
    CustomButton btnAddNew;

    @ViewById(R.id.editFirstName)
    CustomEditText editFirstName;

    @ViewById(R.id.editLastName)
    CustomEditText editLastName;

    @ViewById(R.id.editCurrentPassword)
    CustomEditText editCurrPassword;

    @ViewById(R.id.editNewPassword)
    CustomEditText editNewPassword;

    @ViewById(R.id.editEmail)
    CustomEditText editEmail;

    @ViewById
    CustomEditText editDOB;

    @ViewById(R.id.spinnerGender)
    Spinner spinnerGender;

    DatePickerDialog.OnDateSetListener mDateListener;
    GenderAdapter mGenderAdapter;
    String mGenderId;

    @Touch(R.id.editDOB)
    public boolean dateOfBirth(View view, MotionEvent event)  {
        // open dialog to select date
        DatePickerDialogFragment fragment = DatePickerDialogFragment.newInstance(mDateListener);
        fragment.setListener(mDateListener);
        fragment.show(getActivity().getSupportFragmentManager(),"dialog");
        return false;
    }

    @Click(R.id.btnSave)
    public void saveProfile() {
        if(TextUtils.isEmpty(editEmail.getText().toString()) ||
                TextUtils.isEmpty(editFirstName.getText().toString()) ||
                TextUtils.isEmpty(editLastName.getText().toString()) ||
                TextUtils.isEmpty(editCurrPassword.getText().toString()) ||
                TextUtils.isEmpty(editNewPassword.getText().toString()) ||
                TextUtils.isEmpty(editDOB.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.please_enter_all_fields));
            return;
        }

        if(!ValidationUtils.isNameValid(editFirstName.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.enter_valid_firstname));
            return;
        }

        if(!ValidationUtils.isNameValid(editLastName.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.enter_valid_lastname));
            return;
        }

        if(!ValidationUtils.isValidEmail(editEmail.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.please_enter_valid_email));
            return;
        }

        if(!ValidationUtils.isPasswordValid(editCurrPassword.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.please_enter_valid_password));
            return;
        }

        if(!ValidationUtils.isPasswordValid(editNewPassword.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.please_enter_valid_password));
            return;
        }

        if(!ValidationUtils.isDateValid(editDOB.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.enter_valid_birthdate));
            return;
        }

        // make webservice call to update the user details
        mSdf = new SimpleDateFormat(Global.YYYY_MM_DD);
        User user = new User();
        try {
            Date dt = mSdf.parse(editDOB.getText().toString());
            user.setBirthday(mSdf.format(dt));
            user.setFirstName(editFirstName.getText().toString());
            user.setLastName(editLastName.getText().toString());
            user.setEmail(editEmail.getText().toString());
            user.setPassword(editCurrPassword.getText().toString());
            user.setNewPassword(editNewPassword.getText().toString());
            user.setGenderId(mGenderId);
        }catch (ParseException e ){
            e.printStackTrace();
        }

        if(NetworkUtilities.isNetworkAvailable())  {
            new UpdateProfileAsyncTask(getActivity(), user, mListener).execute();
        }else {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getResources().getString(R.string.no_internet));
            return;
        }
    }

    OrderHistoryAdapter mOrderHistoryAdapter;
    AddressAdapter mAddressAdapter;
    List<Address> mAddressList;
    List<OrderHistory> mOrderHistoryList;

    @Click(R.id.relLeft)
    public void backClicked() {
        getActivity().onBackPressed();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_profile, null);
        setupActionbarFragment(view);
        mListener = this;

        mAddressList =  new ArrayList<>();
        mOrderHistoryList = new ArrayList<>();

        mToolbar.setBackgroundColor(mApp.getResources().getColor(R.color.colorPrimaryOrange));
        mImgBack.setBackgroundResource(R.mipmap.ic_back);

        spinnerGender = (Spinner)view.findViewById(R.id.spinnerGender);

        setGender();
        spinnerGender.setOnItemSelectedListener(this);
        mRelCart.setVisibility(View.INVISIBLE);

        if(NetworkUtilities.isNetworkAvailable()){
            new GetUserDataAsyncTask(getActivity(),mListener).execute();
        }else {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getResources().getString(R.string.no_internet));
        }
        return view;
    }

    @Override
    public void onResponseReceived(Object object) {
        if(object != null) {
            if(object instanceof ProfileResponseHolder) {
                ProfileResponseHolder holder  = (ProfileResponseHolder)object;
                if(holder.getProfileResponse().getStatus().equals("1")){
                    setData(holder.getProfileResponse().getUserProfile());
                }
            }
        }
    }

    private void setData(UserProfile userProfile) {
        mAddressList = userProfile.getAddressList();
        mOrderHistoryList = userProfile.getOrderHistoryList();

        if(mAddressList == null) {
            mAddressList = new ArrayList<>();
        }

        if(mOrderHistoryList == null) {
            mOrderHistoryList = new ArrayList<>();
        }

        mAddressAdapter = new AddressAdapter(GroodApp.getAppContext(),
                R.layout.individual_address, mAddressList);
        listViewAddress.setAdapter(mAddressAdapter);

        mOrderHistoryAdapter = new OrderHistoryAdapter(GroodApp.getAppContext(),
                R.layout.individual_order_history, mOrderHistoryList);
        listViewOrderHistory.setAdapter(mOrderHistoryAdapter);

        if(userProfile.getGender().equalsIgnoreCase("male")) {
            mGenderId = "1";
        }else {
            mGenderId = "2";
        }

        int position = 0;
        for(String str : Arrays.asList(mApp.getResources().getStringArray(R.array.gender))){
            if(str.equalsIgnoreCase(userProfile.getGender())) {
                spinnerGender.setSelected(true);
                spinnerGender.setSelection(position);
                break;
            }
            position ++;
        }

        mSdf  =  new SimpleDateFormat(Global.YYYY_MM_DD, Locale.ENGLISH);
        SimpleDateFormat sdf = new SimpleDateFormat(Global.MM_DD_YYYY);

        try {
            String dtString = sdf.format(mSdf.parse(userProfile.getBirthday()));
            editDOB.setText(dtString);
        }catch (ParseException e){
            e.printStackTrace();
        }

        editEmail.setText(userProfile.getEmail());
        editFirstName.setText(userProfile.getFirstName());
        editLastName.setText(userProfile.getLastName());
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        editDOB.setText((monthOfYear + 1) + "-" + dayOfMonth + "-" + year);
    }

    private void setGender() {
        mGenderAdapter = new GenderAdapter(getActivity(),
                R.layout.individual_gender_row,
                Arrays.asList(mApp.getResources().getStringArray(R.array.gender)));
        spinnerGender.setAdapter(mGenderAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        List<String> strGenderID = Arrays.asList(mApp.getResources().getStringArray(R.array.gender_id));
        mGenderId = strGenderID.get(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
