package com.grood.in.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.common.collect.Iterables;
import com.grood.in.GroodApp;
import com.grood.in.R;
import com.grood.in.activities.ProductDetailActivity_;
import com.grood.in.adapters.ProductAdapter;
import com.grood.in.asynctasks.GetProductListAsyncTask;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.customviews.CustomEditText;
import com.grood.in.customviews.CustomTextView;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.models.Category;
import com.grood.in.models.Product;
import com.grood.in.models.ProductListHolder;
import com.grood.in.util.CommonUtilities;
import com.grood.in.util.ItemCacheUtilities;
import com.grood.in.util.constant.Global;
import com.grood.in.util.network.NetworkUtilities;
import com.grood.in.util.search.ProductListPredicate;
import com.grood.in.util.search.SubcategoryPredicate;
import com.grood.in.util.staticmanager.StaticManager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by USER on 10-08-2015.
 */
@EFragment(R.layout.fragment_product_list)
public class ProductListFragment extends ToolbarFragment implements ResponseListener, CustomDialog.PopupMenuListener{
    private static ProductListFragment_ mFragment;
    private Category mCategory;
    private ResponseListener mListener;
    private ProductAdapter mAdapter;
    private String mCategoryName;
    private String mCategoryId;
    private int totalCount;
    private int startValue;
    private int limit;
    private int previousTotal;
    private boolean loading;
    private CustomDialog.PopupMenuListener mPopupListener;
    private PopupWindow  mPopupWindow;
    private CustomTextView mTxtCategoryName;
    private GridView mProdutGrid;
    private List<Product> mProductListOriginal;
    private boolean mIsSearch;

    @App
    GroodApp groodApp;
    private List<Product> mProductList;
    CustomEditText mEditSearch;

    public static ProductListFragment_ newInstance(Category category,
                                                   String categoryId,
                                                   String categoryName){
        mFragment = new ProductListFragment_();
        Bundle args = new Bundle();
        args.putSerializable(Global.CATEGORY, category);
        args.putString(Global.CATEGORY_NAME, categoryName);
        args.putSerializable(Global.CATEGORY_ID, categoryId);
        mFragment.setArguments(args);
        return  mFragment;
    }


    @Click(R.id.relLeft)
    public void backClicked(){
        getActivity().onBackPressed();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        View view = inflater.inflate(R.layout.fragment_product_list, null);
        setupActionbarFragment(view);

        mEditSearch = (CustomEditText)view.findViewById(R.id.editSearch);

        mProductList = new ArrayList<>();
        mListener = this;
        mPopupListener = this;

        mToolbar.setBackgroundColor(groodApp.getResources().getColor(R.color.colorPrimary));
        mImgBack.setBackgroundResource(R.mipmap.ic_back);

        mTxtHeader.setText(groodApp.getResources().getString(R.string.shop_header));
        mImgLogo.setVisibility(View.GONE);
        mTxtHeader.setVisibility(View.VISIBLE);

        mTxtCategoryName  = (CustomTextView)view.findViewById(R.id.txtCategoryName);
        mProdutGrid = (GridView)view.findViewById(R.id.productList);

        if(getArguments() != null) {
            mCategory = (Category)getArguments().getSerializable(Global.CATEGORY);
            mCategoryName = getArguments().getString(Global.CATEGORY_NAME);
            mCategoryId = getArguments().getString(Global.CATEGORY_ID);
            mTxtCategoryName.setText(mCategoryName);
        }

        mProdutGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //  lead to the detail screen
                Product product = mProductList.get(position);
                Intent intent = new Intent(groodApp,
                        ProductDetailActivity_.class);
                intent.putExtra(Global.INTENT_PRODUCT, product);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        setListener();

        mProdutGrid.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (!mIsSearch) {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                            startValue += limit;
                        }
                    }

                    if (startValue < totalCount) {
                        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleItemCount)) {
                            loading = true;

                            if (loading) {
                                startValue += limit;
                            }

                            new GetProductListAsyncTask(getActivity(),
                                    mCategoryId,
                                    String.valueOf(startValue),
                                    String.valueOf(limit),
                                    mListener).execute();
                        }
                    }
                }
            }
        });

        limit = 10;
        if(NetworkUtilities.isNetworkAvailable()) {
            new GetProductListAsyncTask(getActivity(),
                    mCategoryId,
                    String.valueOf(startValue),
                    String.valueOf(limit),
                    mListener).execute();
        }else {
            startValue= 0;
            ProductListHolder holder = StaticManager.getProducts();
            onResponseReceived(holder);
        }

        return view;
    }

    @Click(R.id.relCart)
    public void showQuickCart() {
        if(mPopupWindow != null  && mPopupWindow.isShowing()) {
            mPopupWindow.dismiss();
        }else {
            mPopupWindow = CustomDialog.showQuickCheckout(getActivity(),
                    mRelCart,
                    mCartListener);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateItemCount();
    }

    @Override
    public void onResponseReceived(Object object) {
        if(object != null) {
            if(object instanceof ProductListHolder) {
                ProductListHolder holder = (ProductListHolder)object;
                if(holder.getResponse().getStatus().equals("1")) {
                    try {
                        totalCount =
                                Integer.parseInt(holder.getResponse().getTotalCount());
                    }catch (NumberFormatException e){
                        e.printStackTrace();
                    }

                    if(startValue == 0)  {
                        mProductListOriginal  =
                                (List<Product>)CommonUtilities.deepClone(holder.getResponse().getProductList());
                        setAdapter(holder.getResponse().getProductList());
                    }else {
                        if(holder.getResponse().getProductList() != null) {
                            mProductList.addAll(holder.getResponse().getProductList());
                            mProductListOriginal  =
                                    (List<Product>)CommonUtilities.deepClone(mProductList);
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                }
            }
        }
    }

    public void onPopupMenuClicked(int menuId, Product product) {
        switch (menuId){
            case R.id.action_add_to_cart:
                // add to cart list
                // or increase the quantity
                ItemCacheUtilities.addToCart(product);
                updateItemCount();
                break;

            case R.id.action_buy_now:
                // add to cart and lead to checkout page
                ItemCacheUtilities.addToCart(product);
                // send to checkout page
                break;
        }
    }

    private void setAdapter(List<Product> productList) {
        if(productList == null) {
            productList = new ArrayList<>();
        }
        mProductList.clear();
        mProductList.addAll(productList);
        mAdapter = new ProductAdapter(getActivity(),
                R.layout.individual_product_item,
                mProductList,
                mPopupListener);
        mProdutGrid.setAdapter(mAdapter);
    }

    private void setListener(){
        mEditSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (!TextUtils.isEmpty(mEditSearch.getText().toString())) {
                        mIsSearch = true;
                        Iterable<Product> products = Iterables.filter(mProductListOriginal,
                                new ProductListPredicate(mEditSearch.getText().toString()));
                        List<Product> newList = Arrays.asList(Iterables.toArray(products, Product.class));
                        setAdapter(newList);
                    } else {
                        mIsSearch = false;
                        setAdapter((List<Product>) CommonUtilities.deepClone(mProductListOriginal));
                    }

                    InputMethodManager inputManager = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        mEditSearch.addTextChangedListener(new MyTextWatcher());
    }

    class MyTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(TextUtils.isEmpty(s.toString())) {
                mIsSearch = false;
                setAdapter((List<Product>)CommonUtilities.deepClone(mProductListOriginal));
            }
        }
    }
}
