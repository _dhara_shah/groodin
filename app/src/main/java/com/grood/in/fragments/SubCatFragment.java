package com.grood.in.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.grood.in.GroodApp;
import com.grood.in.R;
import com.grood.in.activities.ProductActivity;
import com.grood.in.activities.ProductActivity_;
import com.grood.in.adapters.SubCatAdapter;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.customviews.CustomEditText;
import com.grood.in.models.Category;
import com.grood.in.util.CommonUtilities;
import com.grood.in.util.constant.Global;
import com.grood.in.util.search.CategoryPredicate;
import com.grood.in.util.search.SubcategoryPredicate;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by USER on 10-08-2015.
 */
@EFragment(R.layout.fragment_subcategory)
public class SubCatFragment extends ToolbarFragment implements AdapterView.OnItemClickListener {
    private static SubCatFragment_ mFragment;
    private List<Category> mSubList;
    private SubCatAdapter mAdapter;
    private Category mCategory;
    private String mCatId;
    private String mCatName;
    private PopupWindow mPopupWindow;
    private List<Category> mSubListOriginal;
    private CustomEditText mEditSearch;

    @App
    GroodApp mApp;

    ListView mLstView;

    public static SubCatFragment_ newInstance(Category category, String catId, String catName){
        mFragment = new SubCatFragment_();
        Bundle args = new Bundle();
        args.putSerializable(Global.CATEGORY, category);
        args.putString(Global.CATEGORY_ID, catId);
        args.putSerializable(Global.CATEGORY_NAME, catName);
        mFragment.setArguments(args);
        return  mFragment;
    }

    @Click(R.id.relCart)
    public void showQuickCart() {
        if(mPopupWindow != null  && mPopupWindow.isShowing()) {
            mPopupWindow.dismiss();
        }else {
            mPopupWindow = CustomDialog.showQuickCheckout(getActivity(),
                    mRelCart,
                    mCartListener);
        }
    }

    @Click(R.id.relLeft)
    public void backClicked(){
        getActivity().onBackPressed();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateItemCount();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_subcategory, null);
        setupActionbarFragment(view);

        mEditSearch = (CustomEditText)view.findViewById(R.id.editSearch);

        mToolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mImgBack.setBackgroundResource(R.mipmap.ic_back);

        mImgLogo.setVisibility(View.GONE);
        mTxtHeader.setVisibility(View.VISIBLE);

        mSubList = new ArrayList<>();
        mLstView = (ListView)view.findViewById(R.id.subCatList);

        if(getArguments() != null) {
            mCategory = (Category) getArguments().getSerializable(Global.CATEGORY);
            mCatId = getArguments().getString(Global.CATEGORY_ID);
            mCatName = getArguments().getString(Global.CATEGORY_NAME);
            mTxtHeader.setText(mCatName);
            mLstView.setOnItemClickListener(this);
            mSubListOriginal = (List<Category>)CommonUtilities.deepClone(mCategory.getSubSubCategory());
            setAdapter(mCategory.getSubSubCategory());
        }

        setListener();
        return view;
    }

    private void setListener(){
        mEditSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (!TextUtils.isEmpty(mEditSearch.getText().toString())) {
                        Iterable<Category> categoryList = Iterables.filter(mSubListOriginal,
                                new SubcategoryPredicate(mEditSearch.getText().toString()));
                        List<Category> newList = Arrays.asList(Iterables.toArray(categoryList, Category.class));
                        setAdapter(newList);
                    } else {
                        setAdapter((List<Category>) CommonUtilities.deepClone(mSubListOriginal));
                    }
                    InputMethodManager inputManager = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        mEditSearch.addTextChangedListener(new MyTextWatcher());
    }


    private void setAdapter(List<Category> subList) {
        if(subList == null) {
            subList = new ArrayList<>();
        }
        mSubList.clear();
        mSubList.addAll(subList);

        mAdapter =  new SubCatAdapter(getActivity(),
                R.layout.individual_child, mSubList);
        mLstView.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(mApp,
                ProductActivity_.class);
        intent.putExtra(Global.CATEGORY_ID, mCatId);
        intent.putExtra(Global.CATEGORY,mCategory );
        intent.putExtra(Global.CATEGORY_NAME,mCatName );
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    class MyTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(TextUtils.isEmpty(s.toString())) {
                setAdapter((List<Category>)CommonUtilities.deepClone(mSubListOriginal));
            }
        }
    }
}
