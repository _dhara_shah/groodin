package com.grood.in.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.grood.in.R;
import com.grood.in.customviews.CustomTextView;
import com.grood.in.util.CommonUtilities;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by USER on 16-08-2015.
 */
@EFragment(R.layout.layout_side_menu)
public class NavigationFragment extends BaseFragment{
    private static NavigationFragment_ mFragment;
    private NavigationDrawerListener mListener;

    public static NavigationFragment_ newInstance(){
        mFragment = new NavigationFragment_();
        return mFragment;
    }

    public interface NavigationDrawerListener{
        void onNavigationItemClicked(String menuItem, FragmentActivity activity);
    }

    public void setListener(NavigationDrawerListener listener) {
        mListener = listener;
    }

    @ViewById(R.id.txtCategories)
    CustomTextView txtCategories;

    @ViewById(R.id.txtCart)
    CustomTextView txtCart;

    @ViewById(R.id.txtOffers)
    CustomTextView txtOffers;

    @ViewById(R.id.txtAccount)
    CustomTextView txtAccount;

    @ViewById(R.id.txtSettings)
    CustomTextView txtSettings;

    @ViewById(R.id.relRoot)
    RelativeLayout mRootLayout;

    @Click(R.id.txtCategories)
    public void categoriesClicked(){
        if(mListener!=null) {mListener.onNavigationItemClicked(txtCategories.getText().toString(),
                getActivity());}
    }

    @Click(R.id.txtOffers)
    public void offersClicked(){
        if(mListener!=null) {mListener.onNavigationItemClicked(txtOffers.getText().toString(),
                getActivity());}
    }

    @Click(R.id.txtAccount)
    public void accountClicked(){
        if(mListener!=null) {mListener.onNavigationItemClicked(txtAccount.getText().toString(),
                getActivity());}
    }

    @Click(R.id.txtSettings)
    public void settingsClicked(){
        if(mListener!=null) {mListener.onNavigationItemClicked(txtSettings.getText().toString(),
                getActivity());}
    }

    @Click(R.id.txtCart)
    public void cartClicked(){
        if(mListener!=null) {mListener.onNavigationItemClicked(txtCart.getText().toString(),
                getActivity());}
    }

    @AfterViews
    public void afterViews(){
        FrameLayout.LayoutParams params =
                new FrameLayout.LayoutParams((int)((float) CommonUtilities.getScreenWidth() * 0.60),
                        (int)CommonUtilities.getScreenHeight());
        params.gravity = Gravity.LEFT;
        mRootLayout.setLayoutParams(params);
    }

}
