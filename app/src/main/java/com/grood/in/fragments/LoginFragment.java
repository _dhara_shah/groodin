package com.grood.in.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.WindowManager;

import com.grood.in.GroodApp;
import com.grood.in.R;
import com.grood.in.activities.ConfirmOrderActivity_;
import com.grood.in.activities.ForgotPasswordActivity_;
import com.grood.in.activities.HomeActivity;
import com.grood.in.activities.ProfileActivity_;
import com.grood.in.activities.RegistrationActivity_;
import com.grood.in.asynctasks.LoginAsyncTask;
import com.grood.in.asynctasks.SocialLoginAsynctask;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.customviews.CustomEditText;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.models.FacebookBean;
import com.grood.in.models.LoginResponseHolder;
import com.grood.in.models.SocialUser;
import com.grood.in.models.SocialUserResponseHolder;
import com.grood.in.models.TwitterBean;
import com.grood.in.prefs.SharedPrefs;
import com.grood.in.util.ValidationUtils;
import com.grood.in.util.constant.Global;
import com.grood.in.util.network.NetworkUtilities;
import com.twitter.sdk.android.core.TwitterCore;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.w3c.dom.Text;

/**
 * Created by USER on 19-07-2015.
 */
@EFragment(R.layout.fragment_login)
public class LoginFragment extends Fragment implements ResponseListener{
    @App
    GroodApp mApp;

    @Bean
    FacebookBean facebookBean;

    @Bean
    TwitterBean twitterBean;

    @ViewById(R.id.editEmail)
    CustomEditText mEditUsername;

    @ViewById(R.id.editPassword)
    CustomEditText mEditPassword;

    private ResponseListener mListener;
    private String mFrom;

    private static LoginFragment_ mFragment;
    public static LoginFragment_ newInstance(String mFrom) {
        mFragment = new LoginFragment_();
        Bundle args = new Bundle();
        args.putString(Global.INTENT_FROM, mFrom);
        mFragment.setArguments(args);
        return mFragment;
    }

    @AfterViews
    public void afterViews() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        mListener = this;

        if(getArguments() !=  null) {
            mFrom = getArguments().getString(Global.INTENT_FROM);
        }
    }

    @Click(R.id.txtSkip)
    public void skip(){
        if(TextUtils.isEmpty(mFrom)) {
            SharedPrefs.putString(SharedPrefs.USER_ID, "");
            Intent intent= new Intent(mApp, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }else{
            getActivity().onBackPressed();
        }
    }

    @Click(R.id.btnTwitterLogin)
    public void performTwitterLogin() {
        twitterBean.connectTwitter(mFragment);
    }

    @Click(R.id.btnFbLogin)
    public void performFacebookLogin() {
        facebookBean.connectFacebook(mFragment);
    }

    @Click(R.id.txtForgotPassword)
    public void forgotPassword() {
        Intent intent = new Intent(mApp,
                ForgotPasswordActivity_.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Click(R.id.txtSignUp)
    public void register(){
        // send to registration screen
        Intent intent = new Intent(mApp,
                RegistrationActivity_.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Click(R.id.btnSignIn)
    public void signin() {
        if(TextUtils.isEmpty(mEditUsername.getText().toString()) &&
                TextUtils.isEmpty(mEditPassword.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.please_enter_all_fields));
            return;
        }

        if(TextUtils.isEmpty(mEditUsername.getText().toString()) &&
                !TextUtils.isEmpty(mEditPassword.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.please_enter_email));
            return;
        }

        if(!TextUtils.isEmpty(mEditUsername.getText().toString()) &&
                TextUtils.isEmpty(mEditPassword.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.please_enter_password));
            return;
        }

        if(!ValidationUtils.isValidEmail(mEditUsername.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.please_enter_valid_email));
            return;
        }

        if(!ValidationUtils.isPasswordValid(mEditPassword.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.please_enter_valid_password));
            return;
        }

        if(NetworkUtilities.isNetworkAvailable()) {
            new LoginAsyncTask(getActivity(), mEditUsername.getText().toString(),
                    mEditPassword.getText().toString(),
                    mListener).execute();
        }else {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.no_internet));
            return;
        }
    }

    @Override
    public void onResponseReceived(Object object) {
        if(object != null) {
            if(object instanceof LoginResponseHolder) {
                LoginResponseHolder holder = (LoginResponseHolder)object;
                if(holder.getLoginResponse().getStatus().equals("1")) {
                    // save user id to shared prefs

                    if(mFrom != null) {
                        Intent intent = null;
                        if(mFrom.equals(Global.INTENT_FROM_CHECKOUT)) {
                             intent = new Intent(mApp,
                                    ConfirmOrderActivity_.class);
                        }else if(mFrom.equals(Global.INTENT_FROM_PROFILE)) {
                            intent = new Intent(mApp,
                                    ProfileActivity_.class);
                        }
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        getActivity().finish();
                        return;
                    }
                    moveToNextActivity();
                }else {
                    CustomDialog.showAlertDialog(getActivity(),
                            mApp.getString(R.string.login_failure));
                }
            }else if(object instanceof SocialUserResponseHolder)  {
                SocialUserResponseHolder holder=(SocialUserResponseHolder)object;
                if(holder.getResponse().getStatus().equals("1"))  {
                    moveToNextActivity();
                }else{
                    CustomDialog.showAlertDialog(getActivity(),
                            holder.getResponse().getMsg());
                }
            }
        }
    }

    public void socialLoginData(SocialUser socialUser) {
        if(NetworkUtilities.isNetworkAvailable()) {
            new SocialLoginAsynctask(getActivity(),
                    socialUser, mListener).execute();
        }else {
            CustomDialog.showAlertDialog(getActivity(),
                    GroodApp.getAppContext().getString(R.string.no_internet));
            return;
        }
    }

    private void moveToNextActivity(){
        if(TextUtils.isEmpty(mFrom)) {
            Intent intent = new Intent(mApp,
                    HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            getActivity().finish();
        }else {

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == TwitterCore.getInstance().getAuthConfig().getRequestCode()) {
            twitterBean.getTwitterAuthClient().onActivityResult(requestCode, resultCode, data);
        }else {
            facebookBean.onResult(requestCode, resultCode, data);
        }
    }
}
