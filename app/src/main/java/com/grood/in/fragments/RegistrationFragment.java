package com.grood.in.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.Spinner;

import com.grood.in.GroodApp;
import com.grood.in.R;
import com.grood.in.activities.HomeActivity;
import com.grood.in.adapters.GenderAdapter;
import com.grood.in.asynctasks.SignUpAsyncTask;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.customdialogs.DatePickerDialogFragment;
import com.grood.in.customviews.CustomEditText;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.models.SignUpResponse;
import com.grood.in.models.User;
import com.grood.in.util.ValidationUtils;
import com.grood.in.util.constant.Global;
import com.grood.in.util.network.NetworkUtilities;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.Touch;
import org.androidannotations.annotations.ViewById;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by USER on 08-08-2015.
 */
@EFragment(R.layout.fragment_registration)
public class RegistrationFragment extends Fragment implements DatePickerDialog.OnDateSetListener, ResponseListener, AdapterView.OnItemSelectedListener {
    private static RegistrationFragment_ mFragment;

    @App
    GroodApp mApp;

    @ViewById(R.id.editEmail)
    CustomEditText mEditEmail;

    @ViewById(R.id.editPassword)
    CustomEditText mEditPassword;

    @ViewById(R.id.editFirstName)
    CustomEditText mEditFirstName;

    @ViewById(R.id.editLastName)
    CustomEditText mEditLastName;

    @ViewById(R.id.editBirthday)
    CustomEditText mEditBirthday;

    @ViewById(R.id.spinnerGender)
    Spinner mSpinnerGender;

    private GenderAdapter mGenderAdapter;
    private String mGenderId;

    private DatePickerDialog.OnDateSetListener mDateListener;
    private ResponseListener mResponseListener;

    public static RegistrationFragment_ newInstance() {
        mFragment = new RegistrationFragment_();
        return mFragment;
    }

    @AfterViews
    public void afterViews() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        mDateListener = this;
        mResponseListener = this;
        setAdapter();
        mSpinnerGender.setOnItemSelectedListener(this);
    }

    @Touch(R.id.editBirthday)
    public boolean birthdate(View v, MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_UP) {
            // open dialog to select date
            DatePickerDialogFragment fragment = DatePickerDialogFragment.newInstance(mDateListener);
            fragment.setListener(mDateListener);
            fragment.show(getActivity().getSupportFragmentManager(),"dialog");
            return true;
        }
        return false;
    }

    @Click(R.id.btnSignUp)
    public void signUp() {
        if(TextUtils.isEmpty(mEditEmail.getText().toString()) ||
                TextUtils.isEmpty(mEditFirstName.getText().toString()) ||
                TextUtils.isEmpty(mEditLastName.getText().toString()) ||
                TextUtils.isEmpty(mEditPassword.getText().toString()) ||
                TextUtils.isEmpty(mEditBirthday.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.please_enter_all_fields));
            return;
        }

        if(!ValidationUtils.isNameValid(mEditFirstName.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.enter_valid_firstname));
            return;
        }

        if(!ValidationUtils.isNameValid(mEditLastName.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.enter_valid_lastname));
            return;
        }

        if(!ValidationUtils.isValidEmail(mEditEmail.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.please_enter_valid_email));
            return;
        }

        if(!ValidationUtils.isPasswordValid(mEditPassword.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.please_enter_valid_password));
            return;
        }

        if(!ValidationUtils.isDateValid(mEditBirthday.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.enter_valid_birthdate));
            return;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(Global.YYYY_MM_DD, Locale.ENGLISH);
        SimpleDateFormat sdfMonth = new SimpleDateFormat(Global.MM_DD_YYYY, Locale.ENGLISH);

        User user = new User();
        try {
            Date dt = sdfMonth.parse(mEditBirthday.getText().toString());
            user.setBirthday(sdf.format(dt));
            user.setFirstName(mEditFirstName.getText().toString());
            user.setLastName(mEditLastName.getText().toString());
            user.setEmail(mEditEmail.getText().toString());
            user.setPassword(mEditPassword.getText().toString());
            user.setGenderId(mGenderId);
        }catch (ParseException e ){
            e.printStackTrace();
        }

        if(NetworkUtilities.isNetworkAvailable()) {
            new SignUpAsyncTask(getActivity(),
                    user, mResponseListener).execute();
        }else {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getResources().getString(R.string.no_internet));
            return;
        }
    }

    @Click(R.id.txtAlreadyRegistered)
    public void alreadyRegistered() {
        getActivity().onBackPressed();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        mEditBirthday.setText((monthOfYear + 1) + "-" + dayOfMonth + "-" + year);
    }

    @Override
    public void onResponseReceived(Object object) {
        if(object != null) {
            if(object instanceof SignUpResponse) {
                SignUpResponse response = (SignUpResponse)object;
                if(response.getResponse().getStatus().equals("1")) {
                    Intent intent = new Intent(mApp,
                            HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }else {
                    CustomDialog.showAlertDialog(getActivity(),
                            response.getResponse().getMsg());
                }
            }
        }else {

        }
    }

    private void setAdapter() {
        mGenderAdapter = new GenderAdapter(getActivity(),
                R.layout.individual_gender_row,
                Arrays.asList(mApp.getResources().getStringArray(R.array.gender)));
        mSpinnerGender.setAdapter(mGenderAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        List<String> strGenderID = Arrays.asList(mApp.getResources().getStringArray(R.array.gender_id));
        mGenderId = strGenderID.get(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
