package com.grood.in.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.grood.in.GroodApp;
import com.grood.in.R;
import com.grood.in.asynctasks.DownloadFileAsyncTask;
import com.grood.in.asynctasks.ShareAsyncTask;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.customviews.CustomTextView;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.models.FacebookBean;
import com.grood.in.models.Product;
import com.grood.in.models.ShareResponse;
import com.grood.in.models.ShareResponseHolder;
import com.grood.in.models.TwitterBean;
import com.grood.in.util.CommonUtilities;
import com.grood.in.util.ItemCacheUtilities;
import com.grood.in.util.constant.Global;
import com.grood.in.util.network.NetworkUtilities;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by USER on 20-08-2015.
 */
@EFragment(R.layout.fragment_product_desc)
public class ProductDetailFragment extends ToolbarFragment implements ResponseListener{
    private static ProductDetailFragment_ mFragment;
    private Product  mProduct;
    private ResponseListener mListener;

    public static ProductDetailFragment_ newInstance(Product product) {
        mFragment = new ProductDetailFragment_();
        Bundle args = new Bundle();
        args.putSerializable(Global.INTENT_PRODUCT, product);
        mFragment.setArguments(args);
        return mFragment;
    }

    @Bean
    FacebookBean facebookBean;

    @Bean
    TwitterBean twitterBean;

    @ViewById(R.id.txtProductName)
    CustomTextView  txtProductName;

    CustomTextView txtPrice;
    CustomTextView txtDesc;
    CustomTextView txtQty;
    ImageView imgProduct;

    @ViewById(R.id.txtRef)
    CustomTextView txtRef;

    @ViewById(R.id.txtCondition)
    CustomTextView txtCondition;

    @ViewById(R.id.txtPinterest)
    CustomTextView txtPinterest;

    @ViewById(R.id.txtTwtCount)
    CustomTextView txtTwtCount;

    @ViewById(R.id.txtFbCount)
    CustomTextView txtFbCount;

    @App
    GroodApp mApp;

    PopupWindow mPopupWindow;

    @Click(R.id.relPinterest)
    public void shareOnPinterest(){

    }

    @Click(R.id.relTwitter)
    public void shareOnTwitter() {
        if(NetworkUtilities.isNetworkAvailable()){
            new DownloadFileAsyncTask(getActivity(),
                    mProduct.getProductImage(),mListener).execute();
        }else {
            CustomDialog.showAlertDialog(getActivity(),
                    GroodApp.getAppContext().getResources().getString(R.string.no_internet));
            return;
        }
    }

    @Click(R.id.relFb)
    public void shareOnFb() {
        facebookBean.shareOnFacebook(mProduct, mFragment);
    }

    @Click(R.id.minus)
    public void minusClicked() {
        double qty = Double.parseDouble(txtQty.getText().toString());
        qty -= 1;

        if(qty < 1) {
            qty = 1;
        }

        txtQty.setText(String.valueOf(qty));
        mProduct.setProductQuantity(txtQty.getText().toString());
    }

    @Click(R.id.plus)
    public void plusClicked() {
        double qty = Double.parseDouble(txtQty.getText().toString());
        qty += 1;
        txtQty.setText(String.valueOf(qty));
        mProduct.setProductQuantity(txtQty.getText().toString());
    }

    @Click(R.id.btnAddToCart)
    public void addToCart(){
        ItemCacheUtilities.addToCart(mProduct);
        updateItemCount();

        Toast.makeText(mApp,
                "Item added to cart", Toast.LENGTH_SHORT).show();
    }

    @Click(R.id.relCart)
    public void showQuickCart() {
        if(mPopupWindow != null  && mPopupWindow.isShowing()) {
            mPopupWindow.dismiss();
        }else {
            mPopupWindow = CustomDialog.showQuickCheckout(getActivity(),
                    mRelCart,
                    mCartListener);
        }
    }

    @Click(R.id.relLeft)
    public void backClicked(){
        getActivity().onBackPressed();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateItemCount();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_product_desc, null);
        setupActionbarFragment(view);
        mListener = this;
        facebookBean.registerShareDialogCallback(mFragment);

        mToolbar.setBackgroundColor(mApp.getResources().getColor(R.color.colorPrimaryBlue));
        mImgBack.setBackgroundResource(R.mipmap.ic_back);

        mTxtHeader.setTextColor(mApp.getResources().getColor(android.R.color.black));
        mImgLogo.setVisibility(View.GONE);
        mTxtHeader.setVisibility(View.VISIBLE);

        txtPrice = (CustomTextView)view.findViewById(R.id.txtPrice);
        txtQty = (CustomTextView)view.findViewById(R.id.txtQty);
        txtDesc = (CustomTextView)view.findViewById(R.id.txtDesc);
        imgProduct = (ImageView)view.findViewById(R.id.imgProduct);

        if(getArguments() != null) {
            mProduct = (Product)getArguments().getSerializable(Global.INTENT_PRODUCT);

            mTxtHeader.setText(mProduct.getProductName());
            double qty = (((TextUtils.isEmpty(mProduct.getProductQuantity()))
                    ? 1.0 : Double.parseDouble(mProduct.getProductQuantity())));

            if(qty <= 0){
                qty = 1;
            }

            txtQty.setText(""+qty);
            txtDesc.setText(mProduct.getProductDesc());
            txtQty.setEnabled(false);

            CommonUtilities.getAQInstance()
                    .id(imgProduct)
                    .image("https://" + mProduct.getProductImage(), true, true, 200, 0);

            txtPrice.setText(" र "  + mProduct.getProductPrice());
        }
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        facebookBean.onResult(requestCode, resultCode, data);
    }

    public void shareProduct(String shareType){
        if(NetworkUtilities.isNetworkAvailable()){
            new ShareAsyncTask(getActivity(),
                    mProduct.getProductId(),
                    shareType,
                    mListener).execute();
        }else {
            CustomDialog.showAlertDialog(getActivity(),
                    GroodApp.getAppContext().getResources().getString(R.string.no_internet));
            return;
        }
    }

    @Override
    public void onResponseReceived(Object object) {
        if(object != null){
            if(object instanceof ShareResponseHolder){
                ShareResponseHolder holder = (ShareResponseHolder)object;
                if(holder.getResponse().getStatus().equals("1")){
                    txtFbCount.setText(""+holder.getResponse().getData().getFbShareCount());
                    txtTwtCount.setText(""+holder.getResponse().getData().getTwitterShareCount());
                    txtPinterest.setText(""+holder.getResponse().getData().getPinterestShareCount());
                }
            }else if(object instanceof String){
                String filePath = (String)object;
                twitterBean.shareOnTwitter(mProduct, filePath,mFragment);
            }
        }
    }
}
