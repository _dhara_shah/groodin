package com.grood.in.fragments;

import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.WindowManager;

import com.grood.in.GroodApp;
import com.grood.in.R;
import com.grood.in.asynctasks.ForgotpasswordAsyncTask;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.customviews.CustomEditText;
import com.grood.in.interfaces.ResponseListener;
import com.grood.in.models.ForgotPasswordHolder;
import com.grood.in.util.ValidationUtils;
import com.grood.in.util.network.NetworkUtilities;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by USER on 09-08-2015.
 */
@EFragment(R.layout.fragment_forgot_password)
public class ForgotPasswordFragment extends Fragment implements ResponseListener{
    private ResponseListener responseListener;
    private static ForgotPasswordFragment_ mFragment;

    public static ForgotPasswordFragment_ newInstance(){
        mFragment = new ForgotPasswordFragment_();
        return mFragment;
    }

    @App
    GroodApp mApp;

    @ViewById(R.id.editEmail)
    CustomEditText mEditEmail;

    @AfterViews
    public void afterViews() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        responseListener = this;
    }

    @Click(R.id.btnForgotPassword)
    public void forgotPassword() {
        if(TextUtils.isEmpty(mEditEmail.getText().toString())) {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.please_enter_email));
            return;
        }

        if(!ValidationUtils.isValidEmail(mEditEmail.getText().toString()))  {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getString(R.string.please_enter_valid_email));
            return;
        }

        if(NetworkUtilities.isNetworkAvailable()) {
            new ForgotpasswordAsyncTask(getActivity(),
                    mEditEmail.getText().toString(),
                    responseListener).execute();
        }else {
            CustomDialog.showAlertDialog(getActivity(),
                    mApp.getResources().getString(R.string.no_internet));
            return;
        }
    }

    @Override
    public void onResponseReceived(Object object) {
        if(object != null) {
            if(object instanceof ForgotPasswordHolder) {
                ForgotPasswordHolder holder = (ForgotPasswordHolder)object;
                if(holder.getResponse().getStatus().equals("1")) {
                    CustomDialog.showAlertDialog(getActivity(),
                            mApp.getString(R.string.mail_sent),
                            new CustomDialog.DismissListener(){
                                @Override
                                public void onDismissed() {
                                    getActivity().onBackPressed();
                                }
                            });
                }else {
                    CustomDialog.showAlertDialog(getActivity(),
                            holder.getResponse().getMsg());
                    return;
                }
            }
        }
    }
}
