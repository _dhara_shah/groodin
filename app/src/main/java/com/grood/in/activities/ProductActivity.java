package com.grood.in.activities;

import android.support.v4.app.FragmentTransaction;

import com.grood.in.R;
import com.grood.in.fragments.ProductListFragment;
import com.grood.in.fragments.ProductListFragment_;
import com.grood.in.models.Category;
import com.grood.in.util.constant.Global;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

/**
 * Created by USER on 10-08-2015.
 */
@EActivity(R.layout.activity_common)
public class ProductActivity extends ToolbarBaseActivity{
    @AfterViews
    public void afterViews() {
       if(getIntent().getExtras() != null) {
           ProductListFragment_ fragment =
                   ProductListFragment_.newInstance(
                           (Category)getIntent().getSerializableExtra(Global.CATEGORY),
                           getIntent().getStringExtra(Global.CATEGORY_ID),
                           getIntent().getStringExtra(Global.CATEGORY_NAME));
           FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
           ft.replace(R.id.frame_content, fragment);
           ft.commit();
       }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
