package com.grood.in.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.grood.in.R;
import com.grood.in.customviews.CustomTextView;
import com.grood.in.fragments.NavigationFragment;
import com.grood.in.fragments.NavigationFragment_;
import com.grood.in.interfaces.ICartListener;
import com.grood.in.models.CartBean;
import com.grood.in.prefs.SharedPrefs;
import com.grood.in.util.CommonUtilities;
import com.grood.in.util.ItemCacheUtilities;

/**
 * Created by USER on 09-08-2015.
 */
public class ToolbarBaseActivity extends AppCompatActivity implements NavigationFragment.NavigationDrawerListener, ICartListener{
    private NavigationFragment.NavigationDrawerListener mListener;
    protected DrawerLayout mDrawerLayout;
    protected FrameLayout mFrameHoldingList;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private NavigationFragment_ mNavigationFragment;
    protected ICartListener mCartListener;
    protected CustomTextView mTxtItemCount;
    protected RelativeLayout mRelCart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mListener = this;
        mCartListener = this;
    }

    protected void setDrawerListener(int stringValue) {
        mActionBarDrawerToggle = new ActionBarDrawerToggle(
                ((Activity)ToolbarBaseActivity.this),
                mDrawerLayout,
                stringValue,
                stringValue){

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
    }

    protected void setDrawerLayout() {
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mFrameHoldingList = (FrameLayout)findViewById(R.id.fragment_menu);
        mDrawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));
        mDrawerLayout.setBackgroundColor(getResources().getColor(android.R.color.white));

        DrawerLayout.LayoutParams params =
                new DrawerLayout.LayoutParams((int)((float) CommonUtilities
                        .getScreenWidth() * 0.65),
                        DrawerLayout.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.LEFT;
        mFrameHoldingList.setLayoutParams(params);
        mFrameHoldingList.invalidate();
    }

    protected void openCloseDrawer(){
        if(!mDrawerLayout.isDrawerOpen(mFrameHoldingList)) {
            mDrawerLayout.openDrawer(Gravity.LEFT);
        }else{
            mDrawerLayout.closeDrawer(mFrameHoldingList);
        }
    }

    /**
     * Loads the fragment that handles the right menu navigation
     */
    protected void setDrawerMenu() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        // Creating a fragment transaction
        FragmentTransaction ft = fragmentManager.beginTransaction();
        mNavigationFragment = NavigationFragment_.newInstance();
        mNavigationFragment.setListener(mListener);
        ft.replace(R.id.fragment_menu, mNavigationFragment);
        ft.commit();
    }

    @Override
    public void onNavigationItemClicked(String menu, FragmentActivity activity) {
        if(mDrawerLayout.isDrawerOpen(mFrameHoldingList)) {
            mDrawerLayout.closeDrawer(mFrameHoldingList);
        }

        if(menu.equals(getString(R.string.categories_side_menu))) {
            if(!(activity instanceof HomeActivity)) {
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                activity.finish();
            }else {
                ((HomeActivity)activity).setViewPagerSeletion(1);
            }
        }else if(menu.equals(getString(R.string.cart_side_menu))) {
            if(!(activity instanceof ConfirmOrderActivity_)) {
                Intent intent = new Intent(getApplicationContext(), ConfirmOrderActivity_.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        }else if(menu.equals(getString(R.string.offers_side_menu))) {

        }else if(menu.equals(getString(R.string.account_side_menu))) {
            if(!(activity instanceof ProfileActivity_)) {
                if(SharedPrefs.getString(SharedPrefs.USER_ID,"").trim().length() >0){
                    Intent intent = new Intent(getApplicationContext(), ProfileActivity_.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }else {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity_.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            }
        }
    }

    protected void updateItemCount(){
        mTxtItemCount.setText("" + ItemCacheUtilities.getItemList().size());
    }

    @Override
    public void onItemQtyIncreased(CartBean cartBean) {
        ItemCacheUtilities.updateListAndIncrease(cartBean);
        updateItemCount();
    }

    @Override
    public void onItemQtyDecreased(CartBean cartBean) {
        ItemCacheUtilities.updateListAndDecrease(cartBean);
        updateItemCount();
    }

    @Override
    public void onItemDeleted(CartBean cartBean) {
        ItemCacheUtilities.deleteFromCart(cartBean);
        if(mRelCart.getVisibility() == View.VISIBLE) {
            updateItemCount();
        }
    }
}
