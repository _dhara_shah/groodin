package com.grood.in.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.grood.in.R;
import com.grood.in.adapters.GroodFragmentPagerAdapter;
import com.grood.in.customdialogs.CustomDialog;
import com.grood.in.customviews.CustomTextView;
import com.grood.in.fragments.CategoryFragment;
import com.grood.in.fragments.NavigationFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by USER on 09-08-2015.
 */
public class HomeActivity extends ToolbarBaseActivity{
    private static int CURRENT_SELECTION;

    RelativeLayout mRelMenu;
    Toolbar mToolbar;
    ViewPager viewPager;
    TabLayout tabLayout;
    PopupWindow mPopupWindow;
    GroodFragmentPagerAdapter mGroodFragmentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_tabs);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        mTxtItemCount = (CustomTextView)findViewById(R.id.txtItemCount);
        mRelMenu = (RelativeLayout)findViewById(R.id.relLeft);
        mRelCart = (RelativeLayout)findViewById(R.id.relCart);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(null);
        mToolbar.setLogo(null);

        mRelMenu.setClickable(true);
        mRelMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCloseDrawer();
            }
        });

        updateItemCount();

        viewPager  = (ViewPager)findViewById(R.id.viewpager);
        tabLayout = (TabLayout)findViewById(R.id.sliding_tabs);

        CURRENT_SELECTION = 1;
        mGroodFragmentAdapter =
                new GroodFragmentPagerAdapter(getSupportFragmentManager(),
                        getApplicationContext());
        viewPager.setAdapter(mGroodFragmentAdapter);
        tabLayout.setupWithViewPager(viewPager);

        setDrawerLayout();
        setDrawerMenu();
        setDrawerListener(R.string.app_name);

        mRelCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // show dialog  here
                if(mPopupWindow != null && mPopupWindow.isShowing())  {
                    mPopupWindow.dismiss();
                }else{
                    mPopupWindow = CustomDialog.showQuickCheckout(HomeActivity.this,
                            mRelCart,
                            mCartListener);
                }
            }
        });
    }

    public void setViewPagerSeletion(int selection) {

    }
}
