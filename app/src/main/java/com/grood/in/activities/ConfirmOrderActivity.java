package com.grood.in.activities;


import android.support.v4.app.FragmentTransaction;

import com.grood.in.R;
import com.grood.in.fragments.ConfirmOderFragment_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

/**
 * Created by USER on 21-08-2015.
 */
@EActivity(R.layout.activity_common)
public class ConfirmOrderActivity  extends BaseActivity {
    @AfterViews
    public void afterViews(){
        ConfirmOderFragment_ fragment = ConfirmOderFragment_.newInstance();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame_content, fragment);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
