package com.grood.in.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.grood.in.R;
import com.grood.in.fragments.ProductDetailFragment_;
import com.grood.in.models.Product;
import com.grood.in.util.constant.Global;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

/**
 * Created by USER on 20-08-2015.
 */
@EActivity(R.layout.activity_common)
public class ProductDetailActivity extends BaseActivity {
    private Product mProduct;

    @AfterViews
    public void afterViews() {
        if (getIntent().getExtras() != null) {
            mProduct = (Product)getIntent().getSerializableExtra(Global.INTENT_PRODUCT);
        }

        ProductDetailFragment_ fragment = ProductDetailFragment_.newInstance(mProduct);
        FragmentTransaction ft =  getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame_content, fragment );
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
