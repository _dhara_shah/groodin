package com.grood.in.activities;

import android.support.v4.app.FragmentTransaction;

import com.grood.in.R;
import com.grood.in.fragments.ProfileFragment_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

/**
 * Created by USER on 22-08-2015.
 */
@EActivity(R.layout.activity_common)
public class ProfileActivity  extends BaseActivity {
    @AfterViews
    public void afterViews() {
        ProfileFragment_ fragment = ProfileFragment_.newInstance();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame_content, fragment);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
