package com.grood.in.activities;

import android.support.v4.app.FragmentTransaction;

import com.grood.in.R;
import com.grood.in.fragments.SubCatFragment;
import com.grood.in.fragments.SubCatFragment_;
import com.grood.in.models.Category;
import com.grood.in.util.constant.Global;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EFragment;

/**
 * Created by USER on 09-08-2015.
 */
@EActivity(R.layout.activity_common)
public class SubCategoryActivity extends BaseActivity{
    private Category mCategory;
    private String mCategoryId;
    private String mCategoryName;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @AfterViews
    public void afterViews() {
        if(getIntent().getExtras() != null) {
            mCategory = (Category)getIntent().getSerializableExtra(Global.CATEGORY);
            mCategoryId = getIntent().getStringExtra(Global.CATEGORY_ID);
            mCategoryName = getIntent().getStringExtra(Global.CATEGORY_NAME);
        }

        SubCatFragment_ fragment = SubCatFragment_.newInstance(mCategory, mCategoryId,mCategoryName);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame_content, fragment);
        ft.commit();
    }
}
