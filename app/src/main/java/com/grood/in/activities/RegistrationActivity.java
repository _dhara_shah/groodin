package com.grood.in.activities;

import android.support.v4.app.FragmentTransaction;

import com.grood.in.R;
import com.grood.in.fragments.RegistrationFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

/**
 * Created by USER on 08-08-2015.
 */
@EActivity(R.layout.activity_common)
public class RegistrationActivity extends BaseActivity{
    @AfterViews
    public void afterViews(){
        RegistrationFragment fragment = RegistrationFragment.newInstance();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame_content, fragment);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
