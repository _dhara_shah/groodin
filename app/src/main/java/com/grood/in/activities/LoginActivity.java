package com.grood.in.activities;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;

import com.grood.in.R;
import com.grood.in.fragments.LoginFragment;
import com.grood.in.fragments.LoginFragment_;
import com.grood.in.models.FacebookBean;
import com.grood.in.models.TwitterBean;
import com.grood.in.util.constant.Global;
import com.twitter.sdk.android.core.TwitterCore;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;

/**
 * Created by USER on 19-07-2015.
 */
@EActivity(R.layout.activity_common)
public class LoginActivity extends BaseActivity{
    private String mFrom;

    @Bean
    FacebookBean facebookBean;

    @Bean
    TwitterBean twitterBean;

    @AfterViews
    public void afterViews() {
        if(getIntent().getExtras() !=null) {
            mFrom = getIntent().getStringExtra(Global.INTENT_FROM);
        }

        LoginFragment_ fragment = LoginFragment_.newInstance(mFrom);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame_content, fragment);
        ft.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == TwitterCore.getInstance().getAuthConfig().getRequestCode()) {
            twitterBean.getTwitterAuthClient().onActivityResult(requestCode, resultCode, data);
        }else{
            facebookBean.getCallbackManager().onActivityResult(requestCode,resultCode,data);
        }
    }
}
