# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository holds the main code for GroodIn Android app. It has the following features incorporated so far:
- Category and subcategory listing
- Search under category and subcategory listing
- Product listing
- Search under product listing
- Add to cart 
- View description
- Share on Facebook, twitter and pinterest (WIP)
- Login via facebook and twitter
- Guest login
- Checkout
- Order history
- My profile section
- Registration

The following still has to be worked on:
- Addition and updation of addresses
- share on pinterest
- Testing of share via facebook and twitter
- Offer listing
- Pay order
- Product Options

* Version - 1.0.0

### How do I get set up? ###

* Summary of set up
This application is set up using Android Studio 1.2 version. Moving over to version 1.3 caused some issues hence had to degrade. Java sdk 1.7 is used but 1.8 can also be used. Presta webservices have been used which returns data in the form of JSON parsed by GSON.

* Configuration
- App is compatible with Android 2.3 to 5.2 (Lollipop)
- Please refer to the internet inorder to setup Lombok Plugin into Android Studio
- Gradle is used to build the application.

* Dependencies
The following libraries and plugins have been used:
- Lombok plugin that enables automated generation of getters and setters
- Guava collections
- Facebook sdk
- Twitter plugins (Fabric)
- RestOK
- Retrofit (For twitter)
- Appcompat library
- Cardsview library 
- Materialish progress bar
- GSON
- Material Design
- Android Annotations (for automated GUI)

* Database configuration
- There is no database involved, however there is a caching mechanism to store the items added to the cart

* Deployment instructions
- Import this project into android studio and set the gradle version to 2.1.1 or the latest version. Build the project and Run it onto a device. The apk generated will be inside /app/build/outputs/apk/app-debug.apk

### Contribution guidelines ###

* Other guidelines
Please ensure that Java naming standards and also android standards are used.

### Who do I talk to? ###

* Dhara Shah
* Dhaval Kanojiya - for the webservices / the owner of the app